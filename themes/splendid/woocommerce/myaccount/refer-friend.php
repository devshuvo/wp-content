<?php
/**
 * Refer a friend content
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( !is_user_logged_in() ) {
	return;
}

$user_id = get_current_user_id();
$purchased = ( get_user_meta( $user_id, 'free_box_purchased', true ) == 1 ) ? get_user_meta( $user_id, 'free_box_purchased', true ) : 0;
$has_free_box = get_user_meta( $user_id, 'has_free_box', true );
$current_referrals = get_user_meta( $user_id, 'current_referrals', true );

//ppr($purchased);
//ppr($has_free_box);
//ppr($current_referrals);


$affiliate = affwp_get_affiliate();

//ppr($affiliate);

$ref_count = sn_more_to_go_count();

do_action( 'woocommerce_before_account_refer_friend' ); ?>

<div class="sn-referral-content">

	<div class="sn-ref-title">
		<h3>Refer-A-Friend</h3>

	</div>

	<div class="refer-text text-lg">
		<p>Send a friend $5, and get a free box when 3 purchase, and get $5 cash credit for each purchase after that!</p>
	</div>

	<div class="refer-text text-lg">
		<p>You can share your unique link with friends using any of the options below:</p>
	</div>

	<div class="snref-progress text-center">
		<div class="snref-progress-text text-lg"><?php echo sn_more_to_go_count_with_text(); ?></div>

		<ul class="<?php if ( $ref_count == 0 ): echo 'completed'; endif; ?>">

			<?php echo sn_referrer_progress_count("1"); ?>
			<?php echo sn_referrer_progress_count("2"); ?>
			<?php echo sn_referrer_progress_count("3"); ?>
		</ul>
	</div>

	<?php if( !is_affiliate_enabled() ) : ?>
		<div class="sn-form-area" style=" text-align: center; ">
			<div id="proceed-signup">
				<?php add_action( 'affwp_register_fields_before_submit', 'affwp_signup_now_button' ); ?>
				<?php echo do_shortcode('[affiliate_registration redirect="'.wc_get_account_endpoint_url('refer-friend').'"]'); ?>
			</div>
		</div>
	<?php else: ?>
		<?php
		if ( function_exists('get_field') ) {
			$social_share_shortcode = get_field('refer-a-friend_social_shortcode','option');
			$social_share_shortcode_phone = get_field('refer-a-friend_social_shortcode_mobile','option');

			echo "<div class='rf-social hide-on-phone'>";
				echo do_shortcode( $social_share_shortcode );
			echo "</div>";

			echo "<div class='rf-social show-on-phone'>";
				echo do_shortcode( $social_share_shortcode_phone );
			echo "</div>";
		}
		?>
	<?php endif; ?>

	<div class="snref-link-generator">
		<?php //echo do_shortcode( '[affiliate_referral_url url="'.home_url().'"]' ); ?>
	</div>

</div>

<?php do_action( 'woocommerce_after_account_refer_friend' ); ?>
