<?php
/**
 * @internal never define functions inside callbacks.
 * these functions could be run multiple times; this would result in a fatal error.
 */
 
/**
 * custom option and settings
 */
function wsmsp_settings_init() {
 // register a new setting for "wsmsp" page
 register_setting( 'wsmsp', 'wsmsp_options' );
 
 // register a new section in the "wsmsp" page
 add_settings_section(
 'wsmsp_section_developers',
 __( 'Settings', 'wsmsp' ),
 'wsmsp_section_developers_cb',
 'wsmsp'
 );
 
 // register wsmsp_orders
 add_settings_field(
 'wsmsp_field_orders',
 __( 'General Settings', 'wsmsp' ),
 'wsmsp_field_cb',
 'wsmsp',
 'wsmsp_section_developers',
 [
 'class' => 'wsmsp_row wsmsp_row_general',
 'wsmsp_text' => 'wsmsp_field_text',
 'wsmsp_text2' => 'wsmsp_field_text2',
 'wsmsp_number_months' => 'wsmsp_field_number_months',
 ]
 );

}
 
/**
 * register our wsmsp_settings_init to the admin_init action hook
 */
add_action( 'admin_init', 'wsmsp_settings_init' );
 
function wsmsp_section_developers_cb( $args ) { ?>



<?php
}

function wsmsp_field_cb( $args ) {
$options = get_option( 'wsmsp_options' );

$defaulttext = "The first {number} months will be billed upfront today.
<br/><br/>
You will then be billed the 'recurring total' every month starting on the date: {date}";

$defaulttext2 = "for the first {number} months.";

 ?>
 
<div id="general-settings" class="settings-area">
 
	<p>
		<!-- Custom Text -->
		<strong>Custom "Info" Text:</strong><br/>
		<textarea rows="6" cols="50" id="<?php echo esc_attr( $args['wsmsp_text'] ); ?>"
		data-custom="<?php echo esc_attr( $args['wsmsp_custom_data'] ); ?>" name="wsmsp_options[<?php echo esc_attr( $args['wsmsp_text'] ); ?>]"><?php if($options[ $args['wsmsp_text'] ] != "") { echo $options[ $args['wsmsp_text'] ]; } else { echo $defaulttext; } ?></textarea><br/>
		<i>Displayed under the order summary page.
	</p>
	<br/>
	<p>
		<!-- Custom Text -->
		<strong>Custom "Subtotal" Text:</strong><br/>
		<textarea rows="1" cols="50" id="<?php echo esc_attr( $args['wsmsp_text2'] ); ?>"
		data-custom="<?php echo esc_attr( $args['wsmsp_custom_data'] ); ?>" name="wsmsp_options[<?php echo esc_attr( $args['wsmsp_text2'] ); ?>]"><?php if($options[ $args['wsmsp_text2'] ] != "") { echo $options[ $args['wsmsp_text2'] ]; } else { echo $defaulttext2; } ?></textarea><br/>
		<i>Displayed after the cart subtotal.
	</p>
	<br/>
	<br/>Use " <code>{number}</code> " to show the minimum number of months.</i>
	<br/>Use " <code>{date}</code> " to show the date that the normal renewals will begin.</i>
	<br/>User " <code>&lt;br&gt;</code> " for a new line.
	<br/><br/><br/>
	<p>
		<!-- Recent Orders Number -->
		<strong>Number of Months:</strong><br/>
		<input type="number" value="<?php if($options[ $args['wsmsp_number_months'] ]) { echo $options[ $args['wsmsp_number_months'] ]; } else { echo "0"; } ?>" id="<?php echo esc_attr( $args['wsmsp_number_months'] ); ?>" data-custom="<?php echo esc_attr( $args['wsmsp_number_months'] ); ?>" name="wsmsp_options[<?php echo esc_attr( $args['wsmsp_number_months'] ); ?>]"><br/>
		<i>The number of months that must be paid upfront.</i>
	</p>

</div>
	
 <?php
}

/**
 * top level menu
 */
function wsmsp_options_page() {
 // add top level menu page
 add_submenu_page(
 'woocommerce',
 'Minimum Signup Period',
 'Minimum Signup Period',
 'manage_options',
 'wsmsp',
 'wsmsp_options_page_html'
 );
}
 
/**
 * register our wsmsp_options_page to the admin_menu action hook
 */
add_action( 'admin_menu', 'wsmsp_options_page' );
 
/**
 * top level menu:
 * callback functions
 */
function wsmsp_options_page_html() {
 // check user capabilities
 if ( ! current_user_can( 'manage_options' ) ) {
 return;
 }
 
 // add error/update messages
 
 // check if the user have submitted the settings
 // wordpress will add the "settings-updated" $_GET parameter to the url
 if ( isset( $_GET['settings-updated'] ) ) {
 // add settings saved message with the class of "updated"
 add_settings_error( 'wsmsp_messages', 'wsmsp_message', __( 'Settings Saved', 'wsmsp' ), 'updated' );
 }
 
 // show error/update messages
 settings_errors( 'wsmsp_messages' );
 ?>
 <div class="wrap">
 <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
 <form action="options.php" method="post">
 <?php
 // output security fields for the registered setting "wsmsp"
 settings_fields( 'wsmsp' );
 // output setting sections and their fields
 // (sections are registered for "wsmsp", each field is registered to a specific section)
 do_settings_sections( 'wsmsp' );
 // output save settings button
 submit_button( 'Save Settings' );
 ?>
 </form>
 
 <br/>Found a bug or have any suggestions? Please get in touch!
 
 </div>
 <?php
}