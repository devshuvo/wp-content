<?php

//add_action( 'init', 'sn_process_payout', 15, 3 );

function sn_process_payout( $order_id = null ){

	if ( !is_user_logged_in() ) {
		//return;
	}

	if ( $order_id ) {

		$affiliate_wp = affiliate_wp()->referrals->get_by( 'reference', $order_id );

		$affiliate_id = $affiliate_wp ? $affiliate_wp->affiliate_id : null;

	} else {
		$affiliate_id = sn_get_cookie('sn_referred_by');
	}

	if ( affwp_is_active_affiliate($affiliate_id) ) {

		$referrals = affwp_get_affiliate_referral_count( $affiliate_id );
		$aff_referrals_count = affwp_count_referrals( $affiliate_id );

		$user_id = affwp_get_affiliate_user_id( $affiliate_id );

		update_user_meta( $user_id, 'current_referrals', $referrals );
		update_user_meta( $user_id, 'current_referrals2', $aff_referrals_count );

		$purchased = ( get_user_meta( $user_id, 'free_box_purchased', true ) == 1 ) ? get_user_meta( $user_id, 'free_box_purchased', true ) : 0;

		if ( ( $referrals > 2 || $aff_referrals_count > 2 ) && $purchased == 0 ) {
			update_user_meta( $user_id, 'has_free_box', 'yes' );
		} else {
			update_user_meta( $user_id, 'has_free_box', 'no' );

		}

		if ( ( $aff_referrals_count < 4 ) && $purchased == 0 ) {
			// Update the previously created referral
			affiliate_wp()->referrals->update_referral( $affiliate_wp->referral_id, array(
				'amount'       => "0.00",
				'description'  => 'Free Box Credit ('.$aff_referrals_count.'/3)',
				'reference'    => $order_id,
				'context'      => 'woocommerce',
				'currency'      => $affiliate_wp->currency,
				'custom'      => $affiliate_wp->custom,
				'type'      => $affiliate_wp->type,
			) );

			//update_user_meta( $user_id, 'affwp_wc_credit_balance', '0' );
		}

		// the referal paid
		do_action( 'affwp_rcp_mark_referrals_as_paid', $order_id );

		sn_setcookie('referral_reward_num', $referrals );
		sn_setcookie('aff_referrals_count', $aff_referrals_count );

	}

}


//add_action( 'woocommerce_order_status_processing', 'sn_process_payout_on_order', 15, 1 );
//add_action( 'woocommerce_order_status_completed', 'sn_process_payout_on_order', 15, 1 );
//add_action( 'woocommerce_before_thankyou', 'sn_process_payout_on_order', 15, 1 );
add_action( 'woocommerce_thankyou', 'sn_process_payout_on_order', 15, 1 );
function sn_process_payout_on_order( $order_id ){
	sn_setcookie('referral_reward_changed','yes');
	sn_process_payout( $order_id );
}

//add_action( 'affwp_set_referral_status', 'sn_process_payout_trigger', 20, 3 );
//function sn_process_payout_trigger( $referral_id, $new_status, $old_status ){
//	sn_setcookie('referral_reward_changed_ref','ref_statusss');
//	sn_process_payout();
//}


// Check free box availability
function is_available_free_box(){
	if ( !is_user_logged_in() ) {
		return;
	}

	$user_id = get_current_user_id();

	$purchased = ( get_user_meta( $user_id, 'free_box_purchased', true ) == 1 ) ? get_user_meta( $user_id, 'free_box_purchased', true ) : 0;
	$has_free_box = get_user_meta( $user_id, 'has_free_box', true );

	if ( $purchased == 0 && $has_free_box == 'yes' ) {
		return true;
	}

	return false;

}


// Free box purchased
function free_box_purchased_actions( $order_id ){
	// Get an instance of WC_Order object
	$order = wc_get_order( $order_id );

	$user_id = $order->get_customer_id();

	// Coupons used in the order LOOP (as they can be multiple)
	foreach( $order->get_used_coupons() as $coupon_code ){

	    // Retrieving the coupon ID
	    $coupon_post_obj = get_page_by_title($coupon_code, OBJECT, 'shop_coupon');
	    $coupon_id       = $coupon_post_obj->ID;

		$sn_coupon_type = get_post_meta( $coupon_id, 'sn_coupon_type', ture );

	    // Get an instance of WC_Coupon object in an array(necessary to use WC_Coupon methods)
	    //$coupon = new WC_Coupon($coupon_id);

	    // Or use this other conditional method for coupon type
	    if( $sn_coupon_type == 'freebox_freeshipping' ){
	        update_user_meta( $user_id, 'has_free_box', 'no' );
	        update_user_meta( $user_id, 'free_box_purchased', '1' );
	    }
	}
}
//add_action( 'woocommerce_thankyou', 'free_box_purchased_actions', 20, 1 );


// Single box product id
function get_single_box_product_id(){
	return 133;
}

// Free Box Coupon Apply Class
class FreeBoxCouponApplyClass {

	public function __construct() {

		$this->init();

	}

	/**
	 * Get things started
	 *
	 * @access public
	 * @since  2.0.0
	 */
	public function init() {

		add_action( 'sn_myreward_lists', array( $this, 'action_add_checkout_notice' ) );
		add_action( 'woocommerce_cart_loaded_from_session',              array( $this, 'checkout_actions' ) );
		add_action( 'woocommerce_cart_loaded_from_session',              array( $this, 'cart_updated_actions' ) );
		add_action( 'woocommerce_checkout_order_processed',              array( $this, 'validate_coupon_usage' ), 10, 2 );
		//add_filter( 'wcs_renewal_order_created',                         array( $this, 'subscription_actions' ), 10, 2 );
		//add_action( 'woocommerce_subscription_renewal_payment_complete', array( $this, 'subscription_validate_coupon_usage' ) );
		add_action( 'woocommerce_removed_coupon',                        array( $this, 'delete_coupon_on_removal' ) );
	}

	/**
	 * Add a payment to a referrer
	 *
	 * @access protected
	 * @since  0.1
	 * @param  int $referral_id The referral ID
	 * @return bool false if adding failed, object otherwise
	 */
	protected function add_payment( $referral_id ) {

		// Return if the referral ID isn't valid
		if( ! is_numeric( $referral_id ) ) {
			return;
		}

		// Get the referral object
		$referral = affwp_get_referral( $referral_id );

		// Get the user id
		$user_id  = affwp_get_affiliate_user_id( $referral->affiliate_id );

		// Get the user's current woocommerce credit balance
		$current_balance = get_user_meta( $user_id, 'affwp_wc_credit_balance', true );
		$new_balance     = floatval( (float) $current_balance + (float) $referral->amount );

		return update_user_meta( $user_id, 'affwp_wc_credit_balance', $new_balance );
	}

	/**
	 * Edit a store credit payment
	 *
	 * @access protected
	 * @since  0.1
	 * @param  int $referral_id The referral ID
	 * @return bool false if adding failed, object otherwise
	 */
	protected function edit_payment( $referral_id ) {

		// Return if the referral ID isn't valid
		if( ! is_numeric( $referral_id ) ) {
			return;
		}

		// Get the referral object
		$referral   = affwp_get_referral( $referral_id );

		// Get the referral amounts
		$old_amount = $referral->amount;
		$new_amount = $data['amount'];

		// Get the user id
		$user_id    = affwp_get_affiliate_user_id( $referral->affiliate_id );

		// Get the user's current woocommerce credit balance
		$current_balance = get_user_meta( $user_id, 'affwp_wc_credit_balance', true );

		if ( $new_amount > $old_amount ) {
			$new_balance = floatval( $current_balance + $new_amount );

		} elseif ( $new_amount < $old_amount ) {
			$new_balance = floatval( $current_balance - $new_amount );
		}

		return update_user_meta( $user_id, 'affwp_wc_credit_balance', $new_balance );
	}


	/**
	 * Remove a payment from a referrer
	 *
	 * @access protected
	 * @since  0.1
	 * @param  int $referral_id The referral ID
	 * @return bool false if removing failed, object otherwise
	 */
	protected function remove_payment( $referral_id ) {

		// Return if the referral ID isn't valid
		if( ! is_numeric( $referral_id ) ) {
			return;
		}

		// Get the referral object
		$referral = affwp_get_referral( $referral_id );

		// Get the user id
		$user_id  = affwp_get_affiliate_user_id( $referral->affiliate_id );

		// Get the user's current woocommerce credit balance
		$current_balance = get_user_meta( $user_id, 'affwp_wc_credit_balance', true );
		$new_balance     = floatval( $current_balance - $referral->amount );

		return update_user_meta( $user_id, 'affwp_wc_credit_balance', $new_balance );
	}


	/**
	 * Add notice on checkout if user can checkout with coupon
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function action_add_checkout_notice() {

		$cart_coupons   = WC()->cart->get_applied_coupons();
		$coupon_applied = $this->check_for_coupon( $cart_coupons );

		// Control checkout notice display
		$show_checkout_notice = apply_filters( 'show_store_credit_checkout_notice', true );

		// and has not already generated and applied a coupon code
		if( !$coupon_applied && $show_checkout_notice && is_available_free_box() ) {
			?>
			<div class="free_box-wrap">
				<form action="<?php echo esc_url( wc_get_checkout_url() ); ?>">
					<input type="hidden" name="free_box_checkout" value="true">


					<div class="rewards-row">
						<div class="rewards-col"><?php esc_html_e( 'You have a free box with free shipping.', 'splendid' ); ?></div>
						<div class="rewards-col"><button class="button" type="submit"><?php esc_html_e( 'Checkout', 'splendid' ); ?></button></div>
					</div>
				</form>
			</div>
			<?php
		} else {
			?>
			<div class="free_box-wrap">

			</div>
			<?php
		}
	}


	/**
	 * Process checkout actions
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function checkout_actions() {
		if( isset( $_GET['free_box_checkout'] ) && $_GET['free_box_checkout'] ) {
			$user_id           = get_current_user_id();

			// Get the credit balance and cart total
			//$credit_balance    = (float) get_user_meta( $user_id, 'affwp_wc_credit_balance', true );
			//$cart_total        = (float) $this->calculate_cart_subtotal();

			// Determine the max possible coupon value
			//$coupon_total      = $this->calculate_coupon_amount( $credit_balance, $cart_total );
			$coupon_total      = 100;

			// Bail if the coupon value was 0
			if( !is_available_free_box() ) {
				return;
			}

			$product_id = get_single_box_product_id();

			WC()->cart->empty_cart();
			WC()->cart->add_to_cart( $product_id );

			// Attempt to generate a coupon code
			$coupon_code        = $this->generate_coupon( $user_id, $coupon_total );

			// If a coupon code was successfully generated, apply it
			if( $coupon_code ) {
				WC()->cart->add_discount( $coupon_code );
				wp_redirect( remove_query_arg( 'free_box_checkout' ) ); exit;
			}
		}
	}


	/**
	 * Calculate the cart subtotal
	 *
	 * @access protected
	 * @since  0.1
	 * @return float $cart_subtotal The subtotal
	 */
	protected function calculate_cart_subtotal() {
		$cart_subtotal = ( 'excl' == WC()->cart->tax_display_cart ) ? WC()->cart->subtotal_ex_tax : WC()->cart->subtotal;

		return $cart_subtotal;
	}


	/**
	 * Calculate the amount of a coupon
	 *
	 * @access protected
	 * @since  0.1
	 * @param  float $credit_balance The balance of a users account
	 * @param  float $cart_total The value of the current cart
	 * @return float $coupon_amount The coupon amount
	 */
	protected function calculate_coupon_amount( $credit_balance, $cart_total ) {

		// If either of these are empty, return 0
		if( ! $credit_balance || ! $cart_total ) {
			return 0;
		}

		if( $credit_balance > $cart_total ) {
			$coupon_amount  = $cart_total;
		} else {
			$coupon_amount  = $credit_balance;
		}

		return $coupon_amount;
	}

	/**
	 * Set the expiration date of the coupon, if available
	 *
	 * @since  2.1
	 *
	 * @return int|date The future date on which this coupon expires.
	 */
	public function coupon_expires() {

		$expires = date( 'Y-m-d-s', strtotime( '+2 days', current_time( 'timestamp' ) ) );

		return apply_filters( 'affwp_store_credit_expires', $expires );
	}

	/**
	 * Generate a coupon
	 *
	 * @access protected
	 * @since  0.1
	 * @param  int $user_id The ID of a given user
	 * @param  float $amount The amount of the coupon
	 * @return mixed string $coupon_code The coupon code if successful, false otherwise
	 */
	protected function generate_coupon( $user_id = 0, $amount = 100 ){

		//$amount = floatval( $amount );

		if( $amount <= 0 ) {
			return false;
		}

		$affiliate = affiliate_wp()->affiliates->get_by( 'user_id', $user_id );

		if ( $affiliate ) {
			$affiliate_id = $affiliate->affiliate_id;
		}

		if ( ! $affiliate_id ) {
			return false;
		}

		$user_id      = ( $user_id ) ? $user_id : get_current_user_id();
		$user_info    = get_userdata( $user_id );
		$affiliate_id = is_int( $affiliate_id ) ? $affiliate_id : affwp_get_affiliate_id( $user_id );
		$date         = current_time( 'YmdHi' );
		$coupon_code  = 'FREEBOX-FREESHIPPING-' . $user_id . '_' . $date;
		$expires      = $this->coupon_expires();

		// Get the user email and affiliate payment email addresses, to match against customer_email below.
		$user_emails  = array();
		$user_emails  = array(
			$user_info->user_email,
			affwp_get_affiliate_payment_email( $affiliate_id )
		);

		$product_ids = get_single_box_product_id();

		/**
		 * Filters store credit data for coupons.
		 *
		 * @since 2.0
		 * @since 2.3.3 Adds usage count to coupon data.
		 *
		 * @param array $coupon_data The coupon metadata.
		 */
		$coupon_data = apply_filters( 'snfreebox_freeshipping_woocommerce_coupon_data', array(
			'discount_type'    => 'percent',
			'coupon_amount'    => '100',
			'individual_use'   => 'yes',
			'usage_limit'      => '1',
			'usage_count'      => '0',
			'expiry_date'      => $expires,
			'apply_before_tax' => 'yes',
			'free_shipping'    => 'yes',
			'customer_email'   => $user_emails,
			'product_ids'   => $product_ids,
			'usage_limit_per_user'   => "1",
			'limit_usage_to_x_items'   => "1",
			'sn_coupon_type'   => "freebox_freeshipping",
		) );

		$coupon = array(
			'post_title'   => $coupon_code,
			'post_content' => '',
			'post_status'  => 'publish',
			'post_author'  => 1,
			'post_type'    => 'shop_coupon',
			'meta_input'   => $coupon_data
		);

		$new_coupon_id = wp_insert_post( $coupon );

		if ( $new_coupon_id ) {
			return $coupon_code;
		}

		return false;
	}


	/**
	 * Validate a coupon
	 *
	 * @access public
	 * @since  0.1
	 * @param  int     $order_id   The ID of an order
	 * @param  object  $data       The order data
	 * @return void|boolean false  Calls the processed_used_coupon() method if
	 *                             the user ID matches the user ID provided within
	 *
	 */
	public function validate_coupon_usage( $order_id, $data ) {

		// Get the order object
		$order   = new WC_Order( $order_id );

		// Get the user ID associated with the order
		$user_id = $order->get_user_id();

		// Grab an array of coupons used
		$coupons = $order->get_used_coupons();

		// If the order has coupons
		if( $coupon_code = $this->check_for_coupon( $coupons ) ) {

			// Bail if the user ID in the coupon does not match the current user.
			$user_id_from_coupon = intval( substr( $coupon_code, stripos( $coupon_code, '_' ) +1 ) );

			if ( intval( $user_id ) === $user_id_from_coupon ) {
				// Process the coupon usage and remove the amount from the user's credit balance
				$this->process_used_coupon( $user_id, $coupon_code );
			} else {
				return false;
			}
		}
	}


	/**
	 * Check for a coupon
	 *
	 * @access protected
	 * @since  0.1
	 * @param  array $coupons Coupons to check
	 * @return mixed $coupon_code if found, false otherwise
	 */
	public function check_for_coupon( $coupons = array() ) {
		if( ! empty( $coupons ) ) {
			foreach ( $coupons as $coupon_code ) {

				// Return coupon code if an affiliate credit coupon is found
				if ( false !== stripos( $coupon_code, 'FREEBOX-FREESHIPPING-' ) ) {
					return $coupon_code;
				}
			}
		}

		return false;
	}


	/**
	 * Process a used coupon
	 *
	 * @access protected
	 * @since  0.1
	 * @param  int $user_id The ID of a given user
	 * @param  string $coupon_code The coupon to process
	 * @return mixed object if successful, false otherwise
	 */
	protected function process_used_coupon( $user_id = 0, $coupon_code = '' ) {

		if( ! $user_id || ! $coupon_code ) {
			return;
		}

		$coupon        = new WC_Coupon( $coupon_code );
		$coupon_amount = $coupon->get_amount();

		if( ! $coupon_amount ) {
			return;
		}

		// Get the user's current woocommerce credit balance
		$current_balance = get_user_meta( $user_id, 'affwp_wc_credit_balance', true );
		$new_balance     = floatval( $current_balance - $coupon_amount );

		return update_user_meta( $user_id, 'affwp_wc_credit_balance', $new_balance );
	}


	/**
	 * Process subscription renewal actions
	 *
	 * @access public
	 * @since  2.3
	 *
	 * @param object $renewal_order The renewal order object
	 * @param object $subscription  The subscription object
	 *
	 * @return object $renewal_order Renewal order object
	 */
	public function subscription_actions( $renewal_order, $subscription ) {

		$store_credit_woocommerce_subscriptions_enabled = affiliate_wp()->settings->get( 'store-credit-woocommerce-subscriptions' );

		if ( ! $store_credit_woocommerce_subscriptions_enabled ) {
			return $renewal_order;
		}

		if ( ! $renewal_order instanceof WC_Order ) {
			return $renewal_order;
		}

		$user_id = $subscription->get_user_id();

		// Get the credit balance and cart total.
		$credit_balance = (float) get_user_meta( $user_id, 'affwp_wc_credit_balance', true );
		$order_total    = (float) $renewal_order->get_total();

		// Determine the max possible coupon value.
		$coupon_total = $this->calculate_coupon_amount( $credit_balance, $order_total );

		// Bail if the coupon value was 0.
		if ( $coupon_total <= 0 ) {
			return $renewal_order;
		}

		// Attempt to generate a coupon code.
		$coupon_code = $this->generate_coupon( $user_id, $coupon_total );

		if ( $coupon_code ) {
			$renewal_order->apply_coupon( $coupon_code );
		}

		return $renewal_order;
	}


	/**
	 * Validate a coupon for a subscription order
	 *
	 * @access public
	 * @since  2.3
	 *
	 * @param  object $subscription The subscription object
	 *
	 * @return void|false
	 *
	 */
	public function subscription_validate_coupon_usage( $subscription ) {

		$store_credit_woocommerce_subscriptions_enabled = affiliate_wp()->settings->get( 'store-credit-woocommerce-subscriptions' );

		if ( ! $store_credit_woocommerce_subscriptions_enabled ) {
			return;
		}

		$last_order = $subscription->get_last_order( 'all' );

		// Get the user ID associated with the order.
		$user_id = $last_order->get_user_id();

		// Grab an array of coupons used.
		$coupons = $last_order->get_used_coupons();

		// If the order has coupons.
		if ( $coupon_code = $this->check_for_coupon( $coupons ) ) {

			// Bail if the user ID in the coupon does not match the current user.
			$user_id_from_coupon = intval( substr( $coupon_code, stripos( $coupon_code, '_' ) + 1 ) );

			if ( intval( $user_id ) === $user_id_from_coupon ) {
				// Process the coupon usage and remove the amount from the user's credit balance
				$this->process_used_coupon( $user_id, $coupon_code );
			} else {
				return false;
			}

		}

	}

	/**
	 * Update the coupon when a cart action occurs
	 *
	 * @access public
	 * @since  2.3.1
	 *
	 * @return void
	 */
	public function cart_updated_actions() {

		$coupons = WC()->cart->get_applied_coupons();

		if ( $coupon_code = $this->check_for_coupon( $coupons ) ) {

			$cart_total = (float) $this->calculate_cart_subtotal();

			$coupon        = new WC_Coupon( $coupon_code );
			$coupon_amount = (float) $coupon->get_amount();

			// Delete and remove coupon when the cart is emptied
			if ( 0 == $cart_total ) {

				$coupon_id = $coupon->get_id();

				if ( ! empty( $coupon_id ) ) {
					WC()->cart->remove_coupon( $coupon_code );
					wp_delete_post( $coupon_id );
				}

				return;
			}

			// Update coupon amount when the cart is updated
			if ( $cart_total == $coupon_amount ) {
				//return;
			}

			$user_id = get_current_user_id();

			$coupon_id = $coupon->get_id();

			if ( ! empty( $coupon_id ) && is_available_free_box() ) {
				// Update coupon amount with the new max possible coupon value
				update_post_meta( $coupon_id, 'coupon_amount', "100" );
				update_post_meta( $coupon_id, 'free_shipping', "yes" );
			} else {
				update_post_meta( $coupon_id, 'coupon_amount', "0" );
				update_post_meta( $coupon_id, 'free_shipping', "no" );
			}

		}

	}

	/**
	 * Delete a coupon when it is removed
	 *
	 * @access public
	 * @since  2.3.1
	 *
	 * @param string $coupon_code The coupon code
	 *
	 * @return void
	 */
	public function delete_coupon_on_removal( $coupon_code ) {

		if ( false !== stripos( $coupon_code, 'FREEBOX-FREESHIPPING-' ) ) {

			$applied_coupons = isset( WC()->cart->applied_coupons ) ? WC()->cart->applied_coupons : array();

			if ( ! in_array( $coupon_code, $applied_coupons ) ) {

				$coupon    = new WC_Coupon( $coupon_code );
				$coupon_id = $coupon->get_id();

				if ( ! empty( $coupon_id ) ) {
					wp_delete_post( $coupon_id );
				}

			}

		}

	}

}
new FreeBoxCouponApplyClass;
