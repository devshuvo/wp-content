<?php
/**
 * Referral Link Generator
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Affiliates
 * @version 1.0.5
 */

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */


?>

<div class="yith-wcaf yith-wcaf-link-generator woocommerce ss">

	<?php
	if ( function_exists( 'wc_print_notices' ) ) {
		wc_print_notices();
	}
	?>

	<?php do_action( 'yith_wcaf_before_dashboard_section', 'link-generator' ); ?>
	<?php do_action( 'yith_wcaf_before_link_generator' ); ?>


	<ul class="snref-share-wrap">
		<?php if ( $affiliate_id ) : ?>
		<li>
			<div class="snref-share-svg">
				<?php echo splendid_svg('link'); ?>
			</div>
		    <button id="share_link_button" class="button snref-share-button">
		    	<span class="copy_message">Copy</span>
		        <span class="copied_message">Done!</span>
		    </button>
		    <input type="text" id="share_link_input" value="<?php echo esc_url( $referral_link ); ?>" readonly>
		</li>
		<?php endif; ?>
	</ul>




	<!--NAVIGATION MENU-->
	<?php
	$atts = array(
		'show_right_column'    => $show_right_column,
		'show_left_column'     => true,
		'show_dashboard_links' => $show_dashboard_links,
		'dashboard_links'      => $dashboard_links,
	);
	yith_wcaf_get_template( 'navigation-menu.php', $atts, 'shortcodes' )
	?>

	<?php do_action( 'yith_wcaf_after_link_generator' ); ?>

	<?php
	if ( apply_filters( 'yith_wcaf_share_conditions', true ) && $share_enabled ) {
		yith_wcaf_get_template( 'share.php', $share_atts, 'shortcodes' );
	}

	do_action( 'yith_wcaf_after_social_share_template' );
	?>

	<?php do_action( 'yith_wcaf_after_dashboard_section', 'link-generator' ); ?>
</div>
