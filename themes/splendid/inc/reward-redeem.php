<?php
/**
 * Reward Redeem
 */

function sn_myreward_lists(){
	if ( !is_user_logged_in() ) {
		return;
	}

	$user_id = get_current_user_id();
	$user_rewards = get_user_meta( $user_id, 'user_rewards', true );


	echo do_shortcode( '[affiliate_area_referrals]' );

	return;

	?>
	<?php //ppr($user_rewards);?>
	<div class="sn-myreward-lists">
		<?php if( sn_get_rewards() || $user_rewards ) : ?>
			<?php foreach ($user_rewards as $key => $reward) : ?>
				<?php
				$date =  date_i18n( get_option( 'date_format' ), $reward['time'] );
				$available =  ( $reward['available'] == 1 ) ? "Available" : "Used";
				$applied =  isset( $reward['applied']  ) ? $reward['applied'] : null;
				//$applied =   null;

				if ( $reward['type'] == 'free_box' ) {
					$type = "Free Box";
				} elseif ( $reward['type'] == '5_off' ) {
					$type = wc_formatted_price("5") . " Off";
				} elseif ( $reward['type'] == 'free_shipping' ) {
					$type = "Free Shipping";
				}

				if ( $reward['type'] ) :
				?>
				<div class="sn-reward-item">
					<div class="reward-count">#<span class="reward-count-number"><?php echo $key+1; ?></span></div>
					<div class="reward-date">
						<?php echo $date; ?>
					</div>
					<!--<div class="reward-available"><?php echo $available; ?></div>-->
					<div class="reward-type"><?php echo $type; ?></div>
					<div class="reward-button">
						<?php if( $reward['type'] == 'free_box' ) : ?>
							<form class="free_box_apply" method="post">
								<div class="reward-notice"></div>
								<input type="hidden" name="reward-id" value="<?php esc_attr_e( $key ); ?>">
								<input type="hidden" name="add_free_shipping" value="yes">
								<button type="submit" class="button reward-submit">Apply</button>
								<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
							</form>
						<?php else : ?>
							<button type="submit" class="button">Applied</button>
						<?php endif; ?>

						<?php /*
						<?php if( $reward['available'] == 1  ) : ?>
							<?php if ($applied) : ?>
								<button type="submit" class="button reward-submit-applied">Applied</button>
							<?php else : ?>
								<form class="reward-redeem">
									<div class="reward-notice"></div>
									<input type="hidden" name="reward-id" value="<?php esc_attr_e( $key ); ?>">
									<button type="submit" class="button reward-submit">Apply</button>
									<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
								</form>
							<?php endif; ?>
						<?php else: ?>
							<button type="submit" class="button reward-submit-applied">Used</button>
						<?php endif; ?>
						*/?>

					</div>
				</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<?php

}
add_action( 'sn_myreward_lists', 'sn_myreward_lists' );

/**
 * Handles AJAX for user register - referrer
 *
 * @return string
 */
add_action('wp_ajax_reward_redeem_form', 'splendid_reward_redeem_form');
//add_action('wp_ajax_nopriv_reward_redeem_form', 'splendid_reward_redeem_form');
function splendid_reward_redeem_form(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$reward_id = intval( $_POST['reward-id'] );

	if ( isset( $_POST['redirect_to'] ) ) {
    	$response['redirect_to'] = esc_url( $_POST['redirect_to'] );
    }

    if ( $reward_id >= 0 && is_user_logged_in() ) {
		$user_id = get_current_user_id();
		$user_rewards = get_user_meta( $user_id, 'user_rewards', true);
		$user_rewards = $user_rewards && is_array($user_rewards) ? $user_rewards : array();

		//$user_rewards[$reward_id]['available'] = '1';


		if ( $user_rewards[$reward_id]['applied'] != '1' ) {

			$user_rewards[$reward_id]['applied'] = '1';
			update_user_meta( $user_id, 'user_rewards', $user_rewards );

	    	$response['status'] = 'success';
	        $response['notice'] = __( 'Applied Successfully', 'splendid' );

		} else {

			$response['status'] = 'error';
	        $response['notice'] = __( 'Already Applied', 'splendid' );

		}

    } else {
        $response['status'] = 'error';
        $response['notice'] = __( 'Invalid request', 'splendid' );
    }

    //$response['user_rewards'] = $user_rewards;

	// Json Response
	echo wp_json_encode( $response );

	die();
}