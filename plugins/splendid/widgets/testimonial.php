<?php

class SPLENDID_TESTIMONIAL_CLASS extends \Elementor\Widget_Base {


	public function get_name() {
		return 'splendid_testimonial';
	}


	public function get_title() {
		return __( 'Testimonial', 'wpte-addon' );
	}

	public function get_icon() {
		return 'eicon-testimonial';
	}

	public function get_categories() {
		return [ 'splendid' ];
	}

	public function get_keywords() {
		return [ 'hero', 'testimonial', 'splendid', 'sn' ];
	}

    public function get_script_depends() {
        return [ 'owl-carousel', 'splendid-plugin' ];
    }

    public function get_style_depends() {
        return [ 'splendid-plugin' ];
    }


	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'wpte-addon' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);


		$repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'rate',
            [
                'type'    => \Elementor\Controls_Manager::SELECT,
                'label'   => __( 'Stars', 'splendid' ),
                'default' => '5',
                'options' => [
                    '1'  => __('&#9733;','splendid'),
                    '2'  => __('&#9733;&#9733;','splendid'),
                    '3'  => __('&#9733;&#9733;&#9733;','splendid'),
                    '4'  => __('&#9733;&#9733;&#9733;&#9733;','splendid'),
                    '5'  => __('&#9733;&#9733;&#9733;&#9733;&#9733;','splendid'),
                ],
            ]
        );

		$repeater->add_control(
			'description',
			[
				'label' => __( 'Description', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore."', 'splendid' ),
				//'show_label' => false,
			]
		);

		$repeater->add_control(
			'heading',
			[
				'label' => __( 'Name', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '-User', 'splendid' ),
				//'label_block' => true,
			]
		);

		$repeater->add_control(
			'location',
			[
				'label' => __( 'Location', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Location', 'splendid' ),
			]
		);

        $this->add_control(
            'sn_hero_slides',
            [
                'label' => __( 'Slides', 'splendid' ),
                'show_label' => false,
                'prevent_empty' => false,
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ heading }}}',
            ]
        );
        $this->end_controls_section();

	}

    /**
     * Get post type categories.
     */
    private function feedback_ratings( $rate ) {
        if( $rate ){
            echo '<div class="review-stars">';
                for ($i=1; $i <= $rate; $i++) {
                    echo '<img src="'.SPLENDID_IMG_DIR.'/star.svg">';
                }
            echo '</div>';
        }
    }

	protected function render() {

		global $post, $product, $woocommerce;

		$settings = $this->get_settings_for_display();

		$hero_slides = $settings['sn_hero_slides'];

		if( $hero_slides ) :
		?>
		<div class="sn-testimonial-wrap">
			<div class="ast-container">
				<div id="sntt_<?php echo $this->get_id(); ?>" class="sn-testimonial  owl-carousel">
					<?php foreach( $hero_slides as $key => $slide ) : ?>
						<div class="sn-tmslide">
							<?php
							//ppr( $slide );

							$heading = $slide['heading'];
							$description = $slide['description'];
							$location = $slide['location'];
							?>
							<div class="sn_testimonial-caption">
								<?php $this->feedback_ratings( $slide['rate'] ); ?>
	                        	<div class="sntt-desc"><?php echo $description; ?></div>
	                        	<div class="sntt-name-loc">
	                        		<span class="name"><?php echo $heading; ?></span>
	                        		<span class="loc"><?php echo $location; ?></span>
	                        	</div>

	                        </div>

						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<!-- <img src="<?php echo SPLENDID_IMG_DIR; ?>/testimonial_bg_sep.png" class="section_sep"> -->

		<script>
			jQuery('#sntt_<?php echo $this->get_id(); ?>').owlCarousel({
			    //items:3,
			    margin:0,
			    //autoHeight:true,
			    //autoWidth:true,
			    dots:false,
			    nav:true,
			    navText: ["<img src='<?php echo SPLENDID_IMG_DIR; ?>/slider-nav-left.svg'>","<img src='<?php echo SPLENDID_IMG_DIR; ?>/slider-nav-right.svg'>"],
			  	//navText: ["<img src='"+sn_ajax_params.img_dir+"/slider-nav-left.svg'>","<img src='"+sn_ajax_params.img_dir+"/slider-nav-right.svg'>"],

			    loop:true,
			    autoplay:true,
				autoplayTimeout:5000,
				autoplayHoverPause:true,
			    responsive:{
			        0:{
			            items:1,
			        },
			        425:{
			            items:2,
			        },
			        992:{
			            items:3,
			        }
			    }
			});
		</script>
		<?php
		endif;

	}

}