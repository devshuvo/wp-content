<?php
/**
 * /friends redirect to ref
 */
function sn_friends_url_redirect() {

    if ( is_affiliate_enabled() && is_page( 'friends' ) ) {

	    wp_redirect( wc_get_account_endpoint_url('refer-friend') );

        die;
    }

    if ( isset( $_GET['by_ref'] ) && is_page( 'offer-details' ) ) {

    	if ( $_GET['by_ref'] != 'yes' ) {
    		wp_redirect( home_url( '/offer-details-2/' ) );  exit;
    	} else {
    		wp_redirect( remove_query_arg( 'by_ref' ) ); exit;
    	}

    }

}
add_action( 'template_redirect', 'sn_friends_url_redirect' );


