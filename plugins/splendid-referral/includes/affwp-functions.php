<?php
/**
 * Plugin Name: AffiliateWP - Affiliate Status Body Classes
 * Plugin URI: https://affiliatewp.com
 * Description: Adds body classes based on the affiliate's status
 * Author: Drew Jaynes, DrewAPicture
 * Author URI: https://affiliatewp.com
 * Version: 1.0
 */

add_filter( 'body_class', function( $body_classes ) {

	// Make sure AffiliateWP is installed.
	if ( ! function_exists( 'affiliate_wp' ) ) {

		return $body_classes;
	}

	$affiliate = affwp_get_affiliate();

	if ( false !== $affiliate ) {
		// Current logged-in user is an affiliate.
		$body_classes[] = 'affwp-logged-in';

		// Current affiliate status (active, inactive, pending, rejected).
		$body_classes[] = "affwp-{$affiliate->status}-affiliate";
	} else {
		$body_classes[] = 'affwp-logged-out';
	}

	return $body_classes;
}, 200 );


/**
 * Plugin Name: AffiliateWP - Redirect Affiliate Traffic
 * Plugin URI: https://affiliatewp.com
 * Description: Redirects all affiliate traffic to a specific page
 * Author: Andrew Munro
 * Author URI: https://amdrew.com
 * Version: 1.0
 */

/*
 * Any URL that uses the referral variable configured inside AffiliateWP will redirect
 * the user to the page specified in the $location variable below.
 *
 * Example: https://yoursite.com/?ref=123 will redirect to https://yoursite.com/special-promotion/?ref=123
 *
 * Note how the referral variable and value remain intact after the redirection,
 * allowing for the visit to still be recorded inside AffiliateWP.
 *
 * This is especially useful if you are running a temporary promotion and affiliates
 * do not have the time to update their existing referral links (or add new ones)
 */
function affwp_custom_redirect_affiliate_traffic() {

	// The slug of the page where should all referral traffic should link to.
	$location = 'boxdeal';

	// Return early if not using AffiliateWP.
	if ( ! function_exists( 'affiliate_wp' ) ) {
		return;
	}

	// Get the referral variable being used in AffiliateWP.
	$referral_var = affiliate_wp()->tracking->get_referral_var();

	sn_setcookie( 'sn_referred_by', $_GET[$referral_var] );

	// Redirect the user to the location specified, but only if not on the $location page.
	if ( isset( $_GET[$referral_var] ) && is_front_page() ) {

		// Append the referral variable and value to the URL after the redirect.
		$location = add_query_arg( $referral_var, $_GET[$referral_var], $location );

		// Redirect the user to the $location page.
		wp_safe_redirect( $location );
	}

}
add_action( 'template_redirect', 'affwp_custom_redirect_affiliate_traffic' );


/**
 * Referal Landing Page redirect
 */
function sn_referral_landing_redirect() {

	//$ref = get_option( 'yith_wcaf_referral_var_name', true );
	$ref = 'ref';

    if ( isset( $_GET[$ref] ) && is_front_page() ) {
        wp_redirect( home_url( '/boxdeal/?'.$ref.'='.$_GET[$ref] ) );
        die;
    }

}
//add_action( 'template_redirect', 'sn_referral_landing_redirect' );


/**
 * Referal Success Page redirect -- Friends page
 */
function affiliate_registration_redirect_url() {

	return wc_get_account_endpoint_url('refer-friend')."?friends_ref=true";
}

/**
 * Referal Success Page redirect and Coupon -- Friends page
 */
function affiliate_registration_redirect_action() {
	if( isset( $_GET['friends_ref'] ) && $_GET['friends_ref'] ) {
		$user_id           = get_current_user_id();

		if ( get_user_meta( $user_id, 'new_referrer_claimed', true ) != "1" ) {
			// Save data to user meta
			update_user_meta( $user_id, 'new_referrer', "1" );
			update_user_meta( $user_id, 'new_referrer_claimed', "0" );
			update_user_meta( $user_id, 'has_free_shipping', "1" );

		} else {
			die("Cheating? <a href='".home_url( '/' )."'>Back to Home</a>");
		}

		// If successful, redirect without query arg
		if( $user_id ) {
			wp_redirect( remove_query_arg( 'friends_ref' ) ); exit;
		}


	}
}
add_action( 'template_redirect', 'affiliate_registration_redirect_action' );