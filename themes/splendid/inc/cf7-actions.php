<?php

// Check if the email already in database
function is_already_submitted( $form_post_id = null, $name = null, $value = null ){

    global $wpdb;

    $cfdb         = apply_filters( 'cfdb7_database', $wpdb );

    $table_name   = $cfdb->prefix.'db7_forms';

    $orderby = isset($_GET['orderby']) ? 'form_date' : 'form_id';
    $order   = isset($_GET['order']) ? $_GET['order'] : 'desc';
    $order   = esc_sql($order);

    if ( $form_post_id ) {

        $results = $cfdb->get_results( "SELECT form_value FROM $table_name WHERE form_post_id = $form_post_id", OBJECT );

	    foreach ( $results as $result ) {

	    	$form_value = unserialize( $result->form_value );

	    	if( $form_value[$name] == $value && $form_value['offer_taken'] == 'yes' ) {
	    		return true;
	    	}
	    }
	}

	return false;
}

// Check if the email already purchased
function is_already_purchased( $form_post_id = null, $name = null, $value = null ){

    // Check if ever purchased item
    $repeat_purchase = new WC_Prevent_Repeat_Purchases();
	$already_purchased = $repeat_purchase->has_bought_items( $value );

	if ( $already_purchased ) {
		return true;
	}

	return false;
}

add_action( 'init', function(){

	//ppr(is_already_submitted('1638', 'your-email', 'test@gmail.com'));
	//die();
} );

function sn_email_capture_cf7_validate_email($result, $tag) {

    $name = $tag['name'];

	$form = WPCF7_Submission::get_instance();

    if ( $form ) {

        $black_list   = array('_wpcf7', '_wpcf7_version', '_wpcf7_locale', '_wpcf7_unit_tag',
        '_wpcf7_is_ajax_call','cfdb7_name', '_wpcf7_container_post','_wpcf7cf_hidden_group_fields',
        '_wpcf7cf_hidden_groups', '_wpcf7cf_visible_groups', '_wpcf7cf_options','g-recaptcha-response');

        $data           = $form->get_posted_data();

        $rm_underscore  = apply_filters('cfdb7_remove_underscore_data', true);

        $form_data   = array();

        foreach ($data as $key => $d) {

            $matches = array();
            if( $rm_underscore ) preg_match('/^_.*$/m', $key, $matches);

            if ( !in_array($key, $black_list ) && !in_array($key, $uploaded_files ) && empty( $matches[0] ) ) {

                $tmpD = $d;

                if ( ! is_array($d) ){

                    $bl   = array('\"',"\'",'/','\\','"',"'");
                    $wl   = array('&quot;','&#039;','&#047;', '&#092;','&quot;','&#039;');

                    $tmpD = str_replace($bl, $wl, $tmpD );
                }

                $form_data[$key] = $tmpD;
            }

        }

    }

	$wpcf7 = WPCF7_ContactForm::get_current();
	$form_id = $wpcf7->id();

	$errorMessage = "This email is already used";

    if ( $form_data['email_capture_validation'] == 'is_already_submitted' && is_already_submitted( $form_id, $name, $_POST[$name] ) ) {
        $result->invalidate($tag, $errorMessage);
	}

    if ( $form_data['email_capture'] == 'yes' && is_already_purchased( $form_id, $name, $_POST[$name] ) ) {
        $result->invalidate($tag, $errorMessage);
	}

    return $result;
}
add_filter('wpcf7_validate_email*', 'sn_email_capture_cf7_validate_email', 10, 2);



function splendid_before_cf7send_mail( $form_tag, $abort, $submission  ) {


	$form = WPCF7_Submission::get_instance();

    if ( $form ) {

        $black_list   = array('_wpcf7', '_wpcf7_version', '_wpcf7_locale', '_wpcf7_unit_tag',
        '_wpcf7_is_ajax_call','cfdb7_name', '_wpcf7_container_post','_wpcf7cf_hidden_group_fields',
        '_wpcf7cf_hidden_groups', '_wpcf7cf_visible_groups', '_wpcf7cf_options','g-recaptcha-response');

        $data           = $form->get_posted_data();


        $rm_underscore  = apply_filters('cfdb7_remove_underscore_data', true);

        $form_data   = array();

        foreach ($data as $key => $d) {

            $matches = array();
            if( $rm_underscore ) preg_match('/^_.*$/m', $key, $matches);

            if ( !in_array($key, $black_list ) && !in_array($key, $uploaded_files ) && empty( $matches[0] ) ) {

                $tmpD = $d;

                if ( ! is_array($d) ){

                    $bl   = array('\"',"\'",'/','\\','"',"'");
                    $wl   = array('&quot;','&#039;','&#047;', '&#092;','&quot;','&#039;');

                    $tmpD = str_replace($bl, $wl, $tmpD );
                }

                $form_data[$key] = $tmpD;
            }

        }

    }

	$wpcf7 = WPCF7_ContactForm::get_current();
	$form_id = $wpcf7->id();


}

//add_action( 'wpcf7_before_send_mail', 'splendid_before_cf7send_mail', 10, 3 );


// CF7 mail submit hook
function splendid_cf7send_mail_send( $cf7 ) {

	$form = WPCF7_Submission::get_instance();

    if ( $form ) {

        $black_list   = array('_wpcf7', '_wpcf7_version', '_wpcf7_locale', '_wpcf7_unit_tag',
        '_wpcf7_is_ajax_call','cfdb7_name', '_wpcf7_container_post','_wpcf7cf_hidden_group_fields',
        '_wpcf7cf_hidden_groups', '_wpcf7cf_visible_groups', '_wpcf7cf_options','g-recaptcha-response');

        $data           = $form->get_posted_data();


        $rm_underscore  = apply_filters('cfdb7_remove_underscore_data', true);

        $form_data   = array();

        foreach ($data as $key => $d) {

            $matches = array();
            if( $rm_underscore ) preg_match('/^_.*$/m', $key, $matches);

            if ( !in_array($key, $black_list ) && !in_array($key, $uploaded_files ) && empty( $matches[0] ) ) {

                $tmpD = $d;

                if ( ! is_array($d) ){

                    $bl   = array('\"',"\'",'/','\\','"',"'");
                    $wl   = array('&quot;','&#039;','&#047;', '&#092;','&quot;','&#039;');

                    $tmpD = str_replace($bl, $wl, $tmpD );
                }

                $form_data[$key] = $tmpD;
            }

        }

    }

	$wpcf7 = WPCF7_ContactForm::get_current();
	$form_id = $wpcf7->id();

	$by_ref = $form_data['by_ref'];

	// If email capture form
    if ( $form_data['email_capture'] == 'yes' ) {

		// Set email in cookie
		sn_setcookie( 'referrer_email', $form_data['your-email'] );

		// Reset Survey
		if ( $by_ref != 'yes' ) {
			sn_setcookie( 'is_user_done_survey', sn_encrypt('0') );
	    	sn_setcookie( 'is_user_done_survey_claimed', sn_encrypt('0') );
		}

		if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();

    		if ( $by_ref == 'yes' ) {
    			update_user_meta( $user_id, 'new_referrer', '1' );
    			update_user_meta( $user_id, 'new_referrer_claimed', '0' );
    		} elseif ( $by_ref == 'no' ) {
				update_user_meta( $user_id, 'is_user_done_survey', '0' );
    			update_user_meta( $user_id, 'is_user_done_survey_claimed', '0' );
			}


    	} else {
    		if ( $by_ref == 'yes' ) {
    			sn_setcookie( 'new_referrer', sn_encrypt('1') );
    			sn_setcookie( 'new_referrer_claimed', sn_encrypt('0') );
    		} elseif ( $by_ref == 'no' ) {
				sn_setcookie( 'is_user_done_survey', sn_encrypt('0') );
	    		sn_setcookie( 'is_user_done_survey_claimed', sn_encrypt('0') );
			}

    	}
	}

	// If customer survey form
    if ( $form_data['customer_survey_form'] == 'yes' ) {

		if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();

			update_user_meta( $user_id, 'is_user_done_survey', '1' );

			if ( is_user_has_survey_dicount() ) {
				update_user_meta( $user_id, 'is_user_done_survey_claimed', '0' );
			}

    	} else {

			sn_setcookie( 'is_user_done_survey', sn_encrypt('1') );

	    	if ( is_user_has_survey_dicount() ) {
				sn_setcookie( 'is_user_done_survey_claimed', sn_encrypt('0') );
			}


    	}
	}

}

add_action( 'wpcf7_mail_sent', 'splendid_cf7send_mail_send' );

/**
 * Wordpress: Contact form 7, Add form tag (shortcode) current url example
 */
add_action( 'wpcf7_init', 'wpcf7_add_form_tag_current_url' );
function wpcf7_add_form_tag_current_url() {
	// Add shortcode for the form [current_url]
	wpcf7_add_form_tag( 'by_ref',
		'wpcf7_current_url_form_tag_handler',
		array(
			'name-attr' => true
		)
	);
}

// Parse the shortcode in the frontend
function wpcf7_current_url_form_tag_handler( $tag ) {
	global $wp;
	$url = home_url( $wp->request );
	$by_ref = isset($_GET['ref']) ? 'yes' : 'no';


	return '<input type="hidden" name="by_ref" value="'.$by_ref.'" />';
}