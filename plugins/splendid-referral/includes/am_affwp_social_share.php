<?php
add_shortcode( 'am_affwp_social_share', 'am_affwp_social_share_shortcode' );
function am_affwp_social_share_shortcode( $atts ){

	extract( shortcode_atts(
        array(
            'copy_link_enabled' => true,
            'share_facebook_enabled' => false,
            'share_twitter_enabled' => false,
            'share_email_enabled' => false,
            'share_whatsapp_enabled' => false,
            'referral_link' => false,
            'share_link_title' => false,
            'share_twitter_summary' => false,
        ), $atts )
	);

	if ( !$referral_link ) {
		$referral_link = do_shortcode( '[affiliate_referral_url url="'.home_url('/boxdeal').'"]' );
	}

	$share_link_url = $referral_link;

	if ( !$share_link_title ) {
		$share_link_title = "I thought you might be interested in this:";
	}

	if ( !$share_twitter_summary ) {
		$share_twitter_summary = "I thought you might be interested in this:";
	}

	ob_start();
	?>
	<div class="sn-yith-wcaf-share">
		<ul class="snref-share-wrap">
			<?php if ( $copy_link_enabled ) : ?>
			<li>
				<div class="snref-share-svg">
					<?php echo splendid_svg('link'); ?>
				</div>
			    <button id="share_link_button" class="button snref-share-button">
			    	<span class="copy_message">Copy</span>
			        <span class="copied_message">Done!</span>
			    </button>
			    <input type="text" id="share_link_input" value="<?php echo esc_url( $referral_link ); ?>" readonly>
			</li>
			<?php endif; ?>

			<?php if ( $share_facebook_enabled ) : ?>
				<li>
					<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo urlencode( $share_link_url ); ?>&p[title]=<?php echo esc_attr( $share_link_title ); ?>&p[summary]=<?php echo esc_attr( $share_summary ); ?>&quote=<?php echo esc_attr( $share_link_title ); ?>" title="<?php esc_attr_e( 'Facebook', 'yith-woocommerce-affiliates' ); ?>">
					<div class="snref-share-svg">
						<?php echo splendid_svg('facebook'); ?>
					</div>
					<div class="snref-share-button button icon-yith-facebook-official"><?php esc_html_e( 'Post to Facebook', 'splendid' ); ?></div>
					</a>
				</li>
			<?php endif; ?>

			<?php if ( $share_twitter_enabled ) : ?>
				<li>
					<a target="_blank" href="https://twitter.com/share?url=<?php echo urlencode( $share_link_url ); ?>&amp;text=<?php echo esc_attr( $share_twitter_summary ); ?>" title="<?php esc_attr_e( 'Twitter', 'yith-woocommerce-affiliates' ); ?>">
						<div class="snref-share-svg">
							<?php echo splendid_svg('twitter'); ?>
						</div>
						<div class="snref-share-button button icon-yith-twitter"><?php esc_html_e( 'Post to Twitter', 'splendid' ); ?></div>
					</a>
				</li>
			<?php endif; ?>

			<?php if ( $share_pinterest_enabled ) : ?>
				<li>
					<div class="snref-share-svg">
						<?php echo splendid_svg('link'); ?>
					</div>
					<a target="_blank" class="snref-share-button button icon-yith-pinterest-squared" href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode( $share_link_url ); ?>&amp;description=<?php echo esc_attr( $share_summary ); ?>&amp;media=<?php echo esc_attr( $share_image_url ); ?>" title="<?php esc_attr_e( 'Pinterest', 'yith-woocommerce-affiliates' ); ?>" onclick="window.open(this.href); return false;"><?php esc_html_e( 'Post to Pintarest', 'splendid' ); ?></a>
				</li>
			<?php endif; ?>

			<?php if ( $share_email_enabled ) : ?>
				<li>
					<a href="mailto:?subject=<?php echo urlencode( apply_filters( 'yith_wcaf_email_share_subject', $share_link_title ) ); ?>&amp;body=<?php echo esc_attr( apply_filters( 'yith_wcaf_email_share_body', urlencode( $share_link_url ), $share_summary ) ); ?>&amp;title=<?php echo esc_attr( $share_link_title ); ?>" title="<?php esc_attr_e( 'Email', 'yith-woocommerce-affiliates' ); ?>">
					<div class="snref-share-svg">
						<?php echo splendid_svg('mail'); ?>
					</div>
					<div class="snref-share-button button icon-yith-mail-alt"><?php esc_html_e( 'Email Contacts', 'splendid' ); ?></div>
					</a>
				</li>
			<?php endif; ?>

			<?php if ( $share_whatsapp_enabled && wp_is_mobile() ) : ?>
				<li>
					<a href="whatsapp://send?text=<?php echo ! empty( $share_summary ) ? esc_attr( $share_summary ) : esc_attr_e( 'My Referral URL on ', 'yith-woocommerce-affiliates' ) . esc_attr( get_bloginfo( 'name' ) ) . ' - ' . urlencode( $share_link_url ); ?>" data-action="share/whatsapp/share" target="_blank" title="<?php esc_attr_e( 'WhatsApp', 'yith-woocommerce-affiliates' ); ?>">
						<div class="snref-share-svg">
							<?php echo splendid_svg('whatsapp'); ?>
						</div>
						<div class="snref-share-button button icon-yith-whatsapp"><?php esc_html_e( 'Send on Whatsapp', 'splendid' ); ?></div>
					</a>
				</li>

			<?php endif; ?>
			<?php if ( $share_whatsapp_enabled && ! wp_is_mobile() ) : ?>
				<li>
					<a target="_blank" href="https://web.whatsapp.com/send?text=<?php echo ! empty( $share_summary ) ? esc_attr( $share_summary ) : esc_attr_e( 'My Referral URL on ', 'yith-woocommerce-affiliates' ) . esc_attr( get_bloginfo( 'name' ) ) . ' - ' . urlencode( $share_link_url ); ?>" data-action="share/whatsapp/share" title="WhatsApp Web">
						<div class="snref-share-svg">
							<?php echo splendid_svg('whatsapp'); ?>
						</div>
						<div class="snref-share-button button icon-yith-whatsapp"><?php esc_html_e( 'Send on Whatsapp', 'splendid' ); ?></div>
					</a>
				</li>
			<?php endif; ?>
		</ul>
	</div>
	<?php
	return ob_get_clean();

}