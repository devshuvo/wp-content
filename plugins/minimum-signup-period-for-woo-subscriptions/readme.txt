=== Minimum Signup Period For WooCommerce Subscriptions ===
Contributors: ElliotVS,RelyWP
Tags: woocommerce,subscriptions,signup,period,term,minimum,upfront,trial
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LQ59TEPA5PTHE&source=url
Requires at least: 4.7
Tested up to: 5.5
WC tested up to: 4.3.3
Stable tag: trunk
License: GPLv2 or later.

Allows you to create a minimum signup period for the official WooCommerce subscriptions plugin.

== Description ==

Allows you to create a minimum signup period for the official WooCommerce subscriptions plugin.

The customer will pay the full total for the initial minimum period (for example first 3 months), then after that period ends, the subscription will renew as normal each month.

This plugin simply calculates the initial subtotal for the cart based on the minimum signup period, then once the user signs up, the next payment date for the subscription is automatically updated.

Works well with product addons plugins etc.
Does NOT use "trial" period functionality.

<strong>Suggestions or found a bug?</strong>

If you have any suggestions for additional functionality, or have found a bug, please get in touch.

== Installation ==

1. Upload 'woo-sub-minimum-signup-period' to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Configure your settings on the plugins settings page.

== Frequently Asked Questions ==

Will there be more features added in the future?

If you have any suggestions please let me know via my website contact form.

Need support? Feel free to get in touch and I'll be happy to help.

== Screenshots ==

1. Example Checkout (May look different based on your own site/theme settings etc)
2. Settings Page

== Changelog ==

Version 1.0.1<br>
- Added redirect to settings page on activate.

Version 1.0.0<br>
- Initial Release.

== Upgrade Notice ==

None.