<?php
// Adds widget: Splendid Cart
class Splendidcart_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'splendidcart_widget',
			esc_html__( 'Splendid Cart', 'splendid' ),
			array( 'description' => esc_html__( 'Splendid Cart Icon', 'splendid' ), ) // Args
		);
	}

	private $widget_fields = array(
	);

	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		?>
		<div class="sncart">
			<a href="<?php echo home_url('/cart'); ?>">
				<img src="<?php echo SPLENDID_IMG_DIR; ?>/cart.svg">
				<?php echo sn_cart_count(); ?>
			</a>
		</div>
		<?php
		echo $args['after_widget'];
	}

	public function field_generator( $instance ) {
		$output = '';
		foreach ( $this->widget_fields as $widget_field ) {
			$default = '';
			if ( isset($widget_field['default']) ) {
				$default = $widget_field['default'];
			}
			$widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $default, 'splendid' );
			switch ( $widget_field['type'] ) {
				default:
					$output .= '<p>';
					$output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'splendid' ).':</label> ';
					$output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
					$output .= '</p>';
			}
		}
		echo $output;
	}

	public function form( $instance ) {
		$this->field_generator( $instance );
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		foreach ( $this->widget_fields as $widget_field ) {
			switch ( $widget_field['type'] ) {
				default:
					$instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
			}
		}
		return $instance;
	}
}

function register_splendidcart_widget() {
	register_widget( 'Splendidcart_Widget' );
}
add_action( 'widgets_init', 'register_splendidcart_widget' );