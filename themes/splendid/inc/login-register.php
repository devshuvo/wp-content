<?php

/**
 * Handles AJAX user register - Offer Details
 *
 * @return string
 */
add_action('wp_ajax_sn_register_form', 'sn_offer_details_register_ajax_action');
add_action('wp_ajax_nopriv_sn_register_form', 'sn_offer_details_register_ajax_action');
function sn_offer_details_register_ajax_action(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$product_id = 133;
	$ref = get_option( 'yith_wcaf_referral_cookie_name', true );
	$create_reward = false;

	$user_email = sanitize_email( $_POST['email'] );
	$password = isset( $_POST['password'] ) ? $_POST['password'] : null;

	$parts = explode("@", $user_email);
	$user_name = sanitize_text_field( $parts[0] );

    $user_id = username_exists( $username );

    if ( ! $user_id && false == email_exists( $user_email ) ) {
        //$random_password = wp_generate_password( $length = 8, $include_standard_special_chars = false );

    	if ( $password ) {

		    // Create the WordPress User object with the basic required information
	        $user_id = wp_create_user( $user_name, $password, $user_email );

	        if (!$user_id || is_wp_error($user_id)) {
		       $response['status'] = 'error';
		       $response['error_log'] = $user_id->get_error_message();
		    } else {
		    	$response['status'] = 'success';

		    	// Set the global user object
				$current_user = get_user_by( 'id', $user_id );
				// set the WP login cookie
				$secure_cookie = is_ssl() ? true : false;
				wp_set_auth_cookie( $user_id, true, $secure_cookie );

				WC()->cart->empty_cart();
				WC()->cart->add_to_cart( $product_id );

				if ( sn_decrypt(sn_get_cookie('new_referrer')) == "1" ) {

					// Save cookie to user meta
					update_user_meta( $user_id, 'new_referrer', sn_decrypt(sn_get_cookie('new_referrer')) );
					update_user_meta( $user_id, 'new_referrer_claimed', sn_decrypt(sn_get_cookie('new_referrer_claimed')) );
					update_user_meta( $user_id, 'has_free_shipping', "1" );

					// Delect cookie
					sn_delete_cookie('new_referrer');
					sn_delete_cookie('new_referrer_claimed');

					//$create_reward = true;

					// Affiliate Refister
					$user    = (array) get_userdata( $user_id );
					$args    = (array) $user['data'];

					$affiliate_id = affwp_add_affiliate( array(
						'user_id'        => $user_id,
						'payment_email'  => '',
						'status'         => 'active',
						'website_url'    => '',
						'dynamic_coupon' => '',
					) );

					// Retrieve affiliate ID. Resolves issues with caching on some hosts, such as GoDaddy
					$affiliate_id = affwp_get_affiliate_id( $user_id );

					do_action( 'affwp_register_user', $affiliate_id, 'active', $args );


					// load the mailer class
					//$mailer = WC()->mailer();
					//$subject = "Splendid Nutrition: Referral Registration";
					//$message = "Thank you for registration";
					//$content = get_new_referrer_email_html( $subject, $mailer );
					//$headers = "Content-Type: text/html\r\n";
					////send the email through wordpress
					//$mailer->send( $user_email, $subject, $content, $headers );

				}

		    }
	    } else {
	    	$response['status'] = 'error';
        	$response['message'] = __( 'Password is required', 'splendid' );
	    }


    } else {
        $response['status'] = 'error';
        $response['error_log'] = __( 'User already exists. Please login', 'textdomain' );
    }

    if ( isset( $_POST['redirect_to'] ) ) {
    	$response['redirect_to'] = esc_url( $_POST['redirect_to'] );
    }

    if ( $create_reward ) {

		$user_rewards = get_user_meta( $user_id, 'user_rewards', true);
		$user_rewards = $user_rewards && is_array($user_rewards) ? $user_rewards : array();

		$user_rewards[] = array(
		    'type' => '5_off',
		    '_conversion' => '0',
		    'rate' => '5',
		    'available' => '1',
		    'applied' => '0',
		    'order_id' => "",
		    'time' => current_time('timestamp'),
		);

		update_user_meta( $user_id, 'user_rewards', $user_rewards );

    }

	// Json Response
	echo wp_json_encode( $response );

	die();
}


/**
 * Handles AJAX for Register Form - Checkout
 *
 * @return string
 */
add_action('wp_ajax_sn_register_form__checkout', 'sn_checkout_register_ajax_action');
add_action('wp_ajax_nopriv_sn_register_form__checkout', 'sn_checkout_register_ajax_action');
function sn_checkout_register_ajax_action(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$user_email = sanitize_email( $_POST['email'] );
	$random_password = isset( $_POST['password'] ) ? $_POST['password'] : null;
	$newsletter_subscribe = isset( $_POST['newsletter_subscribe'] ) ? true : false;
	$redirect_to = isset( $_POST['redirect_to'] ) ? esc_url( $_POST['redirect_to'] ) : null;

	$parts = explode("@", $user_email);
	$user_name = sanitize_text_field( $parts[0] );

    $user_id = username_exists( $username );

    if ( ! $user_id && false == email_exists( $user_email ) ) {
        //$random_password = wp_generate_password( $length = 8, $include_standard_special_chars = false );

        if ( $random_password ) {

	        // Create the WordPress User object with the basic required information
	        $user_id = wp_create_user( $user_name, $random_password, $user_email );

	        if (!$user_id || is_wp_error($user_id)) {
		       $response['status'] = 'error';
		       $response['message'] = $user_id->get_error_message();
		    } else {

		    	// Set the global user object
				$current_user = get_user_by( 'id', $user_id );
				// set the WP login cookie
				$secure_cookie = is_ssl() ? true : false;
				wp_set_auth_cookie( $user_id, true, $secure_cookie );

				update_user_meta( $user_id, 'newsletter_subscribe', $newsletter_subscribe );

		    	$response['status'] = 'success';
		    	$response['message'] = __( 'Account created. Redirecting...', 'splendid' );

			    if ( $redirect_to ) {
			    	$response['redirect_to'] = $redirect_to;
			    }

		    }
	    } else {
	    	$response['status'] = 'error';
        	$response['message'] = __( 'Password is required', 'splendid' );
	    }

    } else {
        $response['status'] = 'error';
        $response['message'] = __( 'User already exists. Please login', 'splendid' );
    }

	// Json Response
	echo wp_json_encode( $response );

	die();
}




/**
 * Handles AJAX for user login
 *
 */
function sn_ajax_login(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, is_ssl() );
    if ( !is_wp_error($user_signon) ){
        wp_set_current_user($user_signon->ID);
        wp_set_auth_cookie($user_signon->ID);
        echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
    } else {
    	echo json_encode(array('loggedin'=>false, 'message'=>__('Incorrect password')));
    }

    die();
}
add_action( 'wp_ajax_ajaxlogin', 'sn_ajax_login' );
add_action( 'wp_ajax_nopriv_ajaxlogin', 'sn_ajax_login' );

// used in affwp_register_fields_before_submit hook
function affwp_signup_now_button(){
	echo '<p><button type="submit" value="Register">Sign Up Now</button></p>';
}


/**
 * Give 5 Love area
 */
function splendid_give_5_love_area( $type = null ){ ?>

	<div class="giv5-love-area">
		<div class="giv5-love-img">
			<?php if( $type == "give5" ) : ?>
				<img src="<?php echo SPLENDID_IMG_DIR; ?>/bonus-tag.png" class="give5-bonus">
				<div class="give5-bonus hidden">
					<img src="<?php echo SPLENDID_IMG_DIR; ?>/bonus-tag.png?v" class="give5-bonus-img">
					<div class="give5-bonus-text">
						<h4>Bonus:</h4>
						<div class="fapb get-off">Get $<span class="uline">5 off</span> your next order<br> <span class="fapr">&</span> <span class="uline">FREE shipping</span></div>
						<div class="fapr when-you">when you sign up today!</div>
					</div>
				</div>
			<?php endif; ?>

			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-love.svg" class="give5-love hide-on-phone">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-love-mobile.svg" class="give5-love-mobile show-on-phone">

			<div class="login-register-area">
				<div class="signup-htext">
					<h5>send the gift of focus <br>to a distracted friend</h5>
				</div>

				<div class="sn-form-area">

					<?php if( is_user_logged_in() && $type == "give5" ) : ?>
						<div id="proceed-signup">
							<?php add_action( 'affwp_register_fields_before_submit', 'affwp_signup_now_button' ); ?>
							<?php echo do_shortcode('[affiliate_registration redirect="'.affiliate_registration_redirect_url().'"]'); ?>
	                     </div>

					<?php else: ?>
	                     <div id="signup-form" data-toggle-hide>
	                     	<div class="sn-form-header">
	                     		<div class="form-type">
	                     			<p>Create account:</p>
	                     		</div>
	                     		<div class="frmh-text">
	                     			<p>Have an account? <span class="sn-form-toggle" data-toggle-show="#signin-form">Sign in</span></p>
	                     		</div>
	                     	</div>
	                     	<?php if ( is_page( 'friends' ) ) : ?>
	                     		<?php echo do_shortcode('[affiliate_registration redirect="'.affiliate_registration_redirect_url().'"]'); ?>
	                     	<?php else: ?>
		                        <form id="signup" method="post" autocomplete="off">
									<p class="status"></p>
		                            <input id="sn_signup-email" type="email" name="email" placeholder="Email" value="<?php echo sn_get_referrer_email(); ?>" required>
		                            <input id="password" type="password" name="password" placeholder="Password" required>

		                            <div class="text-center">
		                                <button type="submit">Create</button>
		                            </div>

		                            <?php if ( sn_decrypt(sn_get_cookie('new_referrer')) == "1" ) : ?>
		                            	<input type="hidden" name="refer_from" value="boxdeal_ref">
		                        	<?php endif; ?>

		                        	<input type="hidden" name="redirect_to" value="<?php echo wc_get_account_endpoint_url('refer-friend'); ?>?become_an_affiliate=1">

		                            <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
		                        </form>
		                    <?php endif; ?>

	                     </div>

	                     <div id="signin-form" style="display:none" data-toggle-hide>
	                     	<div class="sn-form-header">
	                     		<div class="form-type">
	                     			<p>Sign in:</p>
	                     		</div>
	                     		<div class="frmh-text">
	                     			<p>Want to  <span class="sn-form-toggle" data-toggle-show="#signup-form">create a new account?</span></p>
	                     		</div>
	                     	</div>
	                         <form id="login" method="post" autocomplete="off">
	                             <p class="status"></p>
	                             <input id="username" type="text" name="username" placeholder="Email">
	                             <input id="password" type="password" name="password" placeholder="Password">

	                             <div class="text-center">
	                                 <button class="submit_button" type="submit" value="Login" name="submit">Sign In</button>
	                             </div>

	                             <div class="text-center">
	                                 <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="forgot-pass"><?php esc_html_e( 'Forgot password?', 'splendid' ); ?></a>
	                             </div>

	                             <input type="hidden" name="redirect_to" value="<?php echo wc_get_account_endpoint_url('refer-friend'); ?>">

	                             <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
	                         </form>
	                     </div>
	                 <?php endif; ?>

                 </div>

			</div>
		</div>
	</div>
	<!-- ./giv5-love-area -->
	<?php
}

function splendid_give_5_love_area_bottom( $atts  ){

	extract(
		shortcode_atts( array(
			'classes' => '',
		), $atts )
	);

	ob_start(); ?>
		<div class="give5-bottom-outer  <?php echo $classes; ?>">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/give5-black-shape.png" class="sep">
			<div class="give5-bottom-wrap">
				<div class="give5-container">
					<div class="give5-half-right">
						<div class="give5-bottom-text">
							<?php echo do_shortcode("[hfe_template id='495']"); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	return ob_get_clean();

}
add_shortcode( 'splendid_give_5_love_area_bottom', 'splendid_give_5_love_area_bottom' );