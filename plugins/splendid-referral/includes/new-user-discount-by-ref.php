<?php

function sn_is_new_referrer(){

	if ( is_user_logged_in() ) {

		$user_id = get_current_user_id();

		if ( get_user_meta( $user_id, 'new_referrer', true ) == 1 ) {
			return true;
		}

	} else {
		if ( sn_decrypt(sn_get_cookie('new_referrer')) == 1 ) {
			return true;
		}
	}

	return false;
}


function sn_is_new_referrer_claimed(){

	if ( is_user_logged_in() ) {

		$user_id = get_current_user_id();

		if ( get_user_meta( $user_id, 'new_referrer_claimed', true ) == "0" ) {
			return true;
		}

	} else {
		if ( sn_decrypt( sn_get_cookie( 'new_referrer_claimed') ) == "0" ) {
			return true;
		}
	}

	return false;
}


function sn_is_user_done_survey(){

	if ( is_user_logged_in() ) {

		$user_id = get_current_user_id();

		if ( get_user_meta( $user_id, 'is_user_done_survey', true ) == 1 ) {
			return true;
		}

	} else {
		if ( sn_decrypt(sn_get_cookie('is_user_done_survey')) == 1 ) {
			return true;
		}
	}

	return false;
}

// check survey discount
function is_user_has_survey_dicount(){

	$survey_claimed = false;

	if ( is_user_logged_in() ) {

		$user_id = get_current_user_id();

		if ( get_user_meta( $user_id, 'is_user_done_survey_claimed', true ) != "1" ) {
			$survey_claimed = true;
		}

	} else {
		if ( sn_decrypt( sn_get_cookie( 'is_user_done_survey_claimed') ) != "1" ) {
			$survey_claimed = true;
		}
	}

	if ( $survey_claimed && sn_is_user_done_survey() ) {
		return true;
	}

	return false;

}


// Check if has discount
function sn_new_referrer_has_discount(){

	if ( sn_is_new_referrer() && sn_is_new_referrer_claimed() ) {
		return true;
	}

	if ( is_user_has_survey_dicount() ) {
		return true;
	}

	return false;
}



function get_applied_discount_amount(){
	if ( sn_new_referrer_has_discount() ) {
		return 5;
	}

	return;
}

/**
 * Apply Discount for referrer
 */
add_action( 'woocommerce_cart_calculate_fees', 'sn_referrer_custom_discount', 1000, 1 );
function sn_referrer_custom_discount( $cart ){

	global $woocommerce;

    if ( is_admin() && ! defined( 'DOING_AJAX' ) ){
        return;
    }


    // Only for 2 items or more
    if( sn_new_referrer_has_discount() ){
        //$discount = WC()->cart->get_subtotal() * $percentage / 100;
        //$taxes = array_sum($woocommerce->cart->taxes);

        $discount = get_applied_discount_amount();

        // Apply discount to 2nd item for non on sale items in cart
        if( $discount > 0 ){

        	//$woocommerce->cart->add_fee( sprintf( __("Tax"), wc_formatted_price($taxes)), $taxes, false, '' );

            $woocommerce->cart->add_fee( sprintf( __("Discount (%s off available)"), wc_formatted_price($discount)), -$discount, false, '' );
        }

    }
}


function sn_apply_reward_credit(){

	if ( !get_applied_discount_amount() || is_user_logged_in() ) {
		return;
	}

	if ( sn_decrypt( sn_get_cookie( 'new_referrer_claimed') ) == "1" ) {
		return;
	}


	?>
	<div class="woocommerce-info">
		You have an account balance of <?php echo wc_formatted_price(5); ?>. Would you like to use it now? <a href="<?php echo add_query_arg( 'new_ref_discount', 'true', esc_url( wc_get_checkout_url() ) ); ?>" class="button">Apply</a>
	</div>

	<?php
}
//add_action( 'woocommerce_review_order_before_order_total', 'sn_apply_reward_credit' );
//add_action( 'woocommerce_before_checkout_form', 'sn_apply_reward_credit', 20 );



/**
 * Check user free shipping availability
 */
function sn_user_has_free_shipping(){
	if ( is_user_logged_in() ) {
		$user_id = get_current_user_id();
		if ( get_user_meta( $user_id, 'has_free_shipping', true ) > 0 ) {
			return true;
		}
	}

	return false;
}

/**
 * Free Shipping
 */
add_filter( 'woocommerce_package_rates', 'free_shipping_for_referrer', 20, 2 );
function free_shipping_for_referrer( $rates, $package ) {
	if ( is_admin() && ! defined( 'DOING_AJAX' ) ){
        return;
    }

	if( sn_user_has_free_shipping() ) {

	    foreach( $rates as $rate_key => $rate ){
	        // Excluding free shipping methods
	        if( $rate->method_id != 'free_shipping'){
	            // Set cost to zero
        	    //$rates[$rate_key]->method_id = 'free_shipping';
        	    $rates[$rate_key]->cost = 0;
        	    $rates[$rate_key]->label = 'Free';
	        }
	    }
    }

    return $rates;
}

/**
 * Clear Shipping Rates Cache
 */
add_filter('woocommerce_checkout_update_order_review', 'clear_wc_shipping_rates_cache');
function clear_wc_shipping_rates_cache(){
    $packages = WC()->cart->get_shipping_packages();

    foreach ($packages as $key => $value) {
        $shipping_session = "shipping_for_package_$key";

        unset(WC()->session->$shipping_session);
    }
}


// Five off and Free Shipping
class FiveOffFreeShippingApplyClass{
	public function __construct() {

		$this->init();

	}

	/**
	 * Get things started
	 *
	 * @access public
	 * @since  2.0.0
	 */
	public function init() {

		add_action( 'sn_myreward_lists', array( $this, 'action_add_notice' ) );
		add_action( 'woocommerce_cart_loaded_from_session', array( $this, 'checkout_actions' ) );

	}


	/**
	 * Add notice on checkout if user can checkout with coupon
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function action_add_notice() {

		// Control checkou notice display
		$show_checkout_notice = apply_filters( 'show_off_five_free_shipping_checkout_notice', true );

		// If the user has a credit balance,
		// and has not already generated and applied a coupon code
		if( sn_user_has_free_shipping() && sn_new_referrer_has_discount() && $show_checkout_notice ) {
			?>
			<div class="five-free_shipping-wrap">
				<form action="<?php echo esc_url( wc_get_checkout_url() ); ?>">
					<input type="hidden" name="off_five_free_shipping_checkout" value="true">

					<div class="rewards-row">
						<div class="rewards-col"><?php esc_html_e( 'You have a $5 discount with free shipping.', 'splendid' ); ?></div>
						<div class="rewards-col"><button class="button" type="submit"><?php esc_html_e( 'Checkout', 'splendid' ); ?></button></div>
					</div>
				</form>
			</div>
			<?php
		}
	}


	/**
	 * Process checkout actions
	 *
	 * @access public
	 * @since  0.1
	 * @return void
	 */
	public function checkout_actions() {
		if( isset( $_GET['off_five_free_shipping_checkout'] ) && $_GET['off_five_free_shipping_checkout'] ) {
			$user_id           = get_current_user_id();

			// Bail if the coupon value was 0
			if( !sn_user_has_free_shipping() && !sn_new_referrer_has_discount() ) {
				return;
			}

			$product_id = get_single_box_product_id();

			WC()->cart->empty_cart();
			WC()->cart->add_to_cart( $product_id );

				// If a coupon code was successfully generated, apply it
			if( $product_id ) {
				wp_redirect( remove_query_arg( 'off_five_free_shipping_checkout' ) ); exit;
			}
		}
	}
}

new FiveOffFreeShippingApplyClass;

/**
 * Check for applied coupon
 *
 * @param  array $coupons Coupons to check
 * @return mixed $coupon_code if found, false otherwise
 */
function snwc_affwp_check_for_coupon( $coupons = array() ) {
	if( ! empty( $coupons ) ) {
		foreach ( $coupons as $coupon_code ) {

			// Return coupon code if an affiliate credit coupon is found
			if ( false !== stripos( $coupon_code, 'AFFILIATE-CREDIT-' ) ) {
				return $coupon_code;
			}
		}
	}

	return false;
}


// Store Credit balance Show to my rewards
function affwp_action_add_checkout_notice(){
	$balance        = (float) get_user_meta( get_current_user_id(), 'affwp_wc_credit_balance', true );
	$cart_coupons   = WC()->cart->get_applied_coupons();
	$coupon_applied = snwc_affwp_check_for_coupon( $cart_coupons );

	$notice_subject = __( 'You have an account balance of', 'affiliatewp-store-credit' );
	$notice_query   = __( 'Would you like to use it now?', 'affiliatewp-store-credit' );
	$notice_action  = __( 'Apply', 'affiliatewp-store-credit' );

	// Control checkou notice display
	$show_checkout_notice = apply_filters( 'show_store_credit_myrewards_notice', true );

	// If the user has a credit balance,
	// and has not already generated and applied a coupon code
	if( $balance && !$coupon_applied && $show_checkout_notice ) {
		_e(
			sprintf( '<div class="affwp-credit-wrap"><div class="rewards-row"><div class="rewards-col">%1$s <strong>%2$s</strong>. %3$s </div><div class="rewards-col"><a href="%4$s" class="button">%5$s</a></div></div></div>',
				$notice_subject,
				wc_price( $balance ),
				$notice_query,
				add_query_arg( 'affwp_wc_apply_credit', 'true', esc_url( wc_get_checkout_url() ) ),
				$notice_action
			),
		);
	}
}
add_action('sn_myreward_lists', 'affwp_action_add_checkout_notice', 10);