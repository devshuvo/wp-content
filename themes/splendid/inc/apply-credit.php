<?php

/**
 * Handles AJAX for user register - referrer
 *
 * @return string
 */
add_action('wp_ajax_apply_credit_form', 'splendid_apply_credit_form');
//add_action('wp_ajax_nopriv_reward_redeem_form', 'splendid_reward_redeem_form');
function splendid_apply_credit_form(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$sn_apply_credit = isset( $_POST['sn_apply_credit'] ) ? sanitize_text_field( $_POST['sn_apply_credit'] ) : null;
	$reward_amount = isset( $_POST['reward-amount'] ) ? floatval( sanitize_text_field( $_POST['reward-amount'] ) ) : null;



	if ( isset( $_POST['redirect_to'] ) ) {
    	$response['redirect_to'] = esc_url( $_POST['redirect_to'] );
    }

    if ( $sn_apply_credit && $reward_amount >= 0 && is_user_logged_in() ) {
		$user_id = get_current_user_id();

		if ( $reward_amount <= get_available_discount_amount() ) {

			$response['status'] = 'success';
	        $response['notice'] = __( 'Applied', 'splendid' );

			update_user_meta( $user_id, 'get_applied_discount_amount', $reward_amount );


		} else {

			$response['status'] = 'error';
	        $response['notice'] = __( 'Something went wrong', 'splendid' );

		}

    } else {
        $response['status'] = 'error';

        if ( !$sn_apply_credit ) {
        	$response['notice'] = __( 'Please select apply type', 'splendid' );
        } else {
        	$response['notice'] = __( 'Something went wrong', 'splendid' );
        }

    }


	$reward_amount = get_applied_discount_amount();
    $response['reward_amount'] = $reward_amount;

	// Json Response
	echo wp_json_encode( $response );

	die();
}