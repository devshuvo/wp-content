<?php
/**
 * WooCommerce Product Table Addon
 *
 * @since 1.0.0
 */
class SPLENDID_HERO_CLASS extends \Elementor\Widget_Base {


	public function get_name() {
		return 'splendid_slider';
	}


	public function get_title() {
		return __( 'Hero Slider', 'wpte-addon' );
	}

	public function get_icon() {
		return 'eicon-slides';
	}

	public function get_categories() {
		return [ 'splendid' ];
	}

	public function get_keywords() {
		return [ 'hero', 'slider', 'splendid', 'sn' ];
	}

    public function get_script_depends() {
        return [  'owl-carousel', 'splendid-plugin' ];
    }

    public function get_style_depends() {
        return [ 'splendid-plugin' ];
    }


	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'wpte-addon' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);


		$repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'slide_image', [
                'label' => __( 'Image', 'cafetora' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'dynamic' => [
                    'active' => true,
                ],
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

		$repeater->add_control(
			'heading',
			[
				'label' => __( 'Title', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Lorem ipsum Dolor sit amet', 'splendid' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'description',
			[
				'label' => __( 'Description', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. sed do eiusmod tempor', 'splendid' ),

			]
		);

		$repeater->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Shop Now', 'splendid' ),
			]
		);

		$repeater->add_control(
			'link',
			[
				'label' => __( 'Link', 'splendid' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'splendid' ),
			]
		);

        $this->add_control(
            'sn_hero_slides',
            [
                'label' => __( 'Slides', 'cafetora' ),
                'show_label' => false,
                'prevent_empty' => false,
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ heading }}}',
            ]
        );
        $this->end_controls_section();

	}

	protected function render() {

		global $post, $product, $woocommerce;

		$settings = $this->get_settings_for_display();

		$hero_slides = $settings['sn_hero_slides'];

		if( $hero_slides ) :
		?>
		<div class="sn-hero-slider-wrap">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/hero_shape_1.svg" class="hero_shape_1 hide-on-phone">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/hero_shape_2.svg" class="hero_shape_2 hide-on-phone">

			<!-- <img src="<?php echo SPLENDID_IMG_DIR; ?>/hero_shape_3.svg" class="hero_shape_3 hide-on-phone" data-modal_id="dollar_five_modal"> -->

			<div class="hero_shape_3 hide-on-phone" data-modal_id="dollar_five_modal">
				<?php echo splendid_svg('get_5_pulse'); ?>
			</div>

			<div class="ast-container">
				<div id="snhero_<?php echo $this->get_id(); ?>" class="sn-slider  owl-carousel">
					<?php foreach( $hero_slides as $key => $slide ) : ?>
						<div class="sn-slide">
							<?php
							//ppr( $slide );

							$image_url = $slide['slide_image']['url'];
							$heading = $slide['heading'];
							$description = $slide['description'];
							$button_text = $slide['button_text'];
							$link = $slide['link']['url'];
							?>
							<div class="sn_slider-caption">
	                        	<h2 class="h-large"><?php echo $heading; ?></h2>
	                        	<p class="fapb"><?php echo $description; ?></p>
	                        	<a href="<?php echo $link; ?>" class="button btn-l btn-round br-0 hide-on-phone"><?php echo $button_text; ?></a>
	                        </div>
	                        <div class="sn_slideshow_img">
	                          <img src="<?php echo $image_url; ?>">
							</div>
							<div class="sn_slideshow-btn show-on-phone">
								<a href="<?php echo $link; ?>" class="button btn-l btn-round br-0"><?php echo $button_text; ?></a>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<script>
			jQuery('#snhero_<?php echo $this->get_id(); ?>').owlCarousel({
			    items:1,
			    margin:0,
			    navText: ["<img src='<?php echo SPLENDID_IMG_DIR; ?>/slider-nav-left.svg'>","<img src='<?php echo SPLENDID_IMG_DIR; ?>/slider-nav-right.svg'>"],
			    //navText: ["<img src='"+sn_ajax_params.img_dir+"/slider-nav-left.svg'>","<img src='"+sn_ajax_params.img_dir+"/slider-nav-right.svg'>"],
			    loop:true,
			    //autoHeight:true,
			    //dots:false,
			    autoplay:true,
				autoplayTimeout:7000,
				autoplayHoverPause:true,
			    responsive:{
			        0:{
			            dots:false,
			            nav:true,
			        },
			        768:{
			            dots:true,
			            nav:false,
			        }
			    }
			});
		</script>
		<?php
		endif;

	}

}