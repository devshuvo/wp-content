<?php

function get_customer_survey_email_html( $heading = false, $mailer, $formdata = null ) {

	$template = 'emails/customer-survey.php';

	return wc_get_template_html( $template, array(
		'email_heading' => $heading,
		'sent_to_admin' => false,
		'plain_text'    => false,
		'email'         => $mailer,
		'formdata'         => $formdata,
	) );

}


/**
 * Handles AJAX for user customer suevey
 *
 * @return string
 */
add_action('wp_ajax_customer_survey_form', 'splendid_customer_survey_form_ajax_action');
add_action('wp_ajax_nopriv_customer_survey_form', 'splendid_customer_survey_form_ajax_action');
function splendid_customer_survey_form_ajax_action(){
	// Verify nonce
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	$response = array();

	$ref = get_option( 'yith_wcaf_referral_cookie_name', true );
	$referrer_email_c = isset( $_COOKIE['referrer_email'] ) ? $_COOKIE['referrer_email'] : "";
	$referrer_email_p = isset( $_POST['referrer_email'] ) ? $_POST['referrer_email'] : "";

    if ( $referrer_email_p ) {
    	$response['status'] = 'success';
        $response['notice'] = __( 'Redirecting...', 'textdomain' );

	    $customer_survey = get_option('sn_customer_survey_list', true);
	    $customer_survey = $customer_survey && is_array($customer_survey) ? $customer_survey : array();

		$customer_survey[] = array(
	        'email' => $referrer_email_p,
	        'product_rating' => isset( $_POST['product_rating'] ) ? $_POST['product_rating'] : "",
	        'level_of_focus' => isset( $_POST['level_of_focus'] ) ? $_POST['level_of_focus'] : "",
	        'level_of_focus_comment' => isset( $_POST['level_of_focus_comment'] ) ? $_POST['level_of_focus_comment'] : "",
	        'taste' => isset( $_POST['taste'] ) ? $_POST['taste'] : "",
	        'taste_comment' => isset( $_POST['taste_comment'] ) ? $_POST['taste_comment'] : "",
	        'recommend_friend' => isset( $_POST['recommend_friend'] ) ? $_POST['recommend_friend'] : "",
	        'review' => isset( $_POST['review'] ) ? $_POST['review'] : "",
	        'time' => current_time('timestamp'),
	    );

		update_option( 'sn_customer_survey_list', $customer_survey );

		sn_setcookie( 'is_user_done_survey', sn_encrypt('1') );


		/**
		 * Send HTML emails from WooCommerce.
		 *
		 */

		//$mail_to = 'aktarujjman@gmail.com';
		$mail_to = get_option( 'admin_email' );

		// load the mailer class
		$mailer = WC()->mailer();
		$subject = "Splendid Nutrition: Survey Form";
		$message = "Thank you";
		$content = get_customer_survey_email_html( $subject, $mailer, $_POST );
		$headers = "Content-Type: text/html\r\n";
		//send the email through wordpress
		$mailer->send( $mail_to, $subject, $content, $headers );


    } else {
        $response['status'] = 'error';
        $response['notice'] = __( 'Something went wrong. Try again?', 'textdomain' );
    }

    if ( isset( $_POST['redirect_to'] ) ) {
    	$response['redirect_to'] = esc_url( $_POST['redirect_to'] );
    }

    //$response['customer_survey'] = get_option('sn_customer_survey_list', true);


	// Json Response
	echo wp_json_encode( $response );

	die();
}

// Select star
function cs_star_select( $field = null ){
	?>
	<div class="cs_star_select">
		<input type="text" name="<?php echo esc_attr( $field ); ?>" class="csfield_input screen-reader-text" id="csfield_<?php echo esc_attr( $field ); ?>" required>
		<div class="cs_stars">
			<?php for( $i = 1; $i <= 5; $i++ ) : ?>
				<span class="cs_star" data-value="<?php echo esc_attr( $i ); ?>">
					<svg width="47.934" height="45.662" version="1.1" viewBox="0 0 12.683 12.081" xmlns="http://www.w3.org/2000/svg"> <g transform="translate(-512.36 -1403.8)"> <path transform="matrix(.56054 0 0 .56 287.11 739.6)" d="m413.15 1186.5c0.63626 0 2.3773 7.0348 2.892 7.4088s7.7433-0.1441 7.9399 0.461c0.19661 0.6052-5.9559 4.4349-6.1526 5.04-0.19661 0.6051 2.5299 7.3198 2.0151 7.6938-0.51475 0.3739-6.0582-4.294-6.6945-4.294s-6.1798 4.6679-6.6945 4.294c-0.51475-0.374 2.2117-7.0887 2.0151-7.6938s-6.3492-4.4348-6.1526-5.04c0.19662-0.6051 7.4252-0.087 7.9399-0.461s2.2558-7.4088 2.892-7.4088z" fill="transparent" stroke="#1a1a1a" stroke-width=".94449"/> </g> </svg>
				</span>
			<?php endfor; ?>
		</div>
	</div>
	<?php
}

/**
 * Customer Survey HTML
 */
function customer_survey_form_shortcode( $atts ){

	// get sender email
	if ( is_user_logged_in() ) {
		$customer_email = get_option( 'admin_email' );
	} elseif ( isset( $_COOKIE['referrer_email'] ) ){
		$customer_email = $_COOKIE['referrer_email'];
	} else {
		$customer_email = 'guest@mail.com';
	}

	ob_start();	?>

	<form id="customer_survey_form">

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("1"); ?>
			</div>
			<div class="cs_field_area">
				<p>Did you like Splendid Focus overall? How would you rate the product?</p>
				<?php cs_star_select('product_rating'); ?>
			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("2"); ?>
			</div>
			<div class="cs_field_area">
				<p>How did the product work for you? Did it it have an effect on your level of focus, mood, or mental energy?</p>
				<div class="sn_has_optional">
					<div class="sn_optional_rating">
						<?php cs_star_select('level_of_focus'); ?>
					</div>
					<div class="sn_optional_field">
						<textarea name="level_of_focus_comment" class="sn_textarea" placeholder="Add a comment (optional)"></textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("3"); ?>
			</div>
			<div class="cs_field_area">
				<p>How did it taste?</p>
				<div class="sn_has_optional">
					<div class="sn_optional_rating">
						<?php cs_star_select('taste'); ?>
					</div>
					<div class="sn_optional_field">
						<textarea name="taste_comment" class="sn_textarea" placeholder="Add a comment (optional)"></textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("4"); ?>
			</div>
			<div class="cs_field_area">
				<p>Would you recommend Splendid Focus to a friend?</p>

				<div class="cs_radio_select">
					<label>
						<input class="screen-reader-text" type="radio" name="recommend_friend" value="Yes" required>
						<div class="cs_select_text">Yes</div>
					</label>
					<label>
						<input class="screen-reader-text" type="radio" name="recommend_friend" value="No" required>
						<div class="cs_select_text">No</div>
					</label>
				</div>

			</div>
		</div>

		<div class="cs_form_row clear">
			<div class="cs_svg">
				<?php echo splendid_number_svg("5"); ?>
			</div>
			<div class="cs_field_area">
				<p>Please leave a brief review of 100 characters or more:</p>
				<textarea name="review" class="sn_textarea sn_textarea_review" placeholder="Review"></textarea>
			</div>
		</div>

		<input type="hidden" name="customer_email" value="<?php echo esc_attr( $customer_email ); ?>">

		<!-- <input type="hidden" name="redirect_to" value="<?php echo home_url( '/offer-details-2/' ); ?>">
		<?php //wp_nonce_field( 'ajax-login-nonce', 'security' ); ?> -->

		<div class="notice"></div>
		<div class="cs_form_row clear">
			<div class="cs_field_area w100 text-center">
				<button type="submit" class="button cs_submit noradius">Complete!</button>
			</div>
		</div>

	</form>

	<?php
	return ob_get_clean();
}
add_shortcode( 'customer_survey_form', 'customer_survey_form_shortcode' );