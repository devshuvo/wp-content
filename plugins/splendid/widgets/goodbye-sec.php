<?php
/**
 * WooCommerce Product Table Addon
 *
 * @since 1.0.0
 */
class GOODBYE_DISCOVER_CLASS extends \Elementor\Widget_Base {


	public function get_name() {
		return 'goodbye_splendid';
	}


	public function get_title() {
		return __( 'Goodbye, brain fog', 'wpte-addon' );
	}

	public function get_icon() {
		return 'eicon-slides';
	}

	public function get_categories() {
		return [ 'splendid' ];
	}

	public function get_keywords() {
		return [ 'hero', 'slider', 'splendid', 'goodbuye', 'sn' ];
	}

    public function get_script_depends() {
        return [ 'splendid-plugin' ];
    }

    public function get_style_depends() {
        return [ 'splendid-plugin' ];
    }


	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'wpte-addon' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'heading',
			[
				'label' => __( 'Title', 'splendid' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Goodbye, brain fog.', 'splendid' ),
				'label_block' => true,
			]
		);

        $this->end_controls_section();

	}

	protected function render() {

		global $post, $product, $woocommerce;

		$settings = $this->get_settings_for_display();

		$heading = $settings['heading'];

		if( $heading ) :
		?>
		<div class="sn-goodbye-wrap doparallax">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/goodbye-left-img.svg" class="goodbye-left-img hide-on-phone">
			<img src="<?php echo SPLENDID_IMG_DIR; ?>/goodbye-right-img.svg" class="goodbye-right-img">

			<!-- <img src="<?php echo SPLENDID_IMG_DIR; ?>/goodbye-scroll-img.svg" class="goodbye-scroll-img">-->

			<div class="ast-container">
				<div class="sn-goodbye-inner text-center">
					<h2><?php _e( $heading ); ?></h2>
				</div>
			</div>
		</div>
		<?php
		endif;

	}

}