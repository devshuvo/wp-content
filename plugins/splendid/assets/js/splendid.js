

(function($){
	'use strict'
	$(document).ready(function($){
		console.log('hello from Plugin');

		function splendid_plugin_init(){



		}

		splendid_plugin_init();
		$(document).on('splendid_plugin_init', function(){
			splendid_plugin_init();
		});

		// Support for elementor editor mode
		if( typeof elementorFrontend !== 'undefined'  ){
			try{
				elementorFrontend.hooks.addAction( 'frontend/element_ready/widget', function( $scope ) {
					splendid_plugin_init();
	            });
			} catch( e ){
				console.log(e);
			}
        }



		var winScrollTop=0;

		$.fn.is_on_screen = function(){
		    var win = $(window);
		    var viewport = {
		        top : win.scrollTop(),
		        left : win.scrollLeft()
		    };
		    //viewport.right = viewport.left + win.width();
		    viewport.bottom = viewport.top + win.height();

		    var bounds = this.offset();
		    //bounds.right = bounds.left + this.outerWidth();
		    bounds.bottom = bounds.top + this.outerHeight();

		    return (!(viewport.bottom < bounds.top || viewport.top > bounds.bottom));
		};

		function parallax() {
		  var scrolled = $(window).scrollTop();
		  $('.parallax-section').each(function(){

		  	 if ($(this).is_on_screen()) {
		  	 			var firstTop = $(this).offset().top;
		          var $span = $(this).find(".moving-block");
		          var moveTop = (firstTop-winScrollTop)*0.1 //speed;
		          $span.css("transform","translateY("+moveTop+"px)");
		     }

		  });
		}

		$(window).scroll(function(e){
		  winScrollTop = $(this).scrollTop();
		  parallax();
		});

		$(window).on("scroll", function() {
			var spos = $(document).scrollTop();

	    });


	});
})(jQuery)