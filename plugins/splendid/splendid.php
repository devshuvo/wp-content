<?php
/**
 * Plugin Name: Splendid Core
 * Description: Custom Plugin for Splendid
 * Plugin URI:  https://www.splendidnutrition.com/
 * Version:     1.0.0
 * Author:      Shuvo
 * Author URI:  https://www.splendidnutrition.com/
 * Text Domain: splendid
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
if ( ! defined( 'SPLENDID_PLUGIN_URL' ) ) {
	define( 'SPLENDID_PLUGIN_URL', plugins_url( '/', __FILE__ ));
}

// Let's load Photoswipe DOM
function is_product(){
	if ( is_front_page() || is_page( 'boxdeal' ) ) {
		return true;
	} else {
		return is_singular( array( 'product' ) );
	}
}

/**
 * Main Elementor Test Extension Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class SPLENDID_MAIN_CLASS {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const CRS_ADDON_VERSION = '1.0.10';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '7.0';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 * @var Elementor_Test_Extension The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 * @static
	 *
	 * @return Elementor_Test_Extension An instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {

		add_action( 'init', [ $this, 'i18n' ] );
		add_action( 'plugins_loaded', [ $this, 'init' ] );

	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function i18n() {

		load_plugin_textdomain( 'splendid' );

	}

	/**
	 * Register Scripts
	 */
	function register_scripts(){

		$SPLENDID_MAIN_CLASS__CRS_ADDON_VERSION = current_time( 'timestamp' );

	    wp_register_style( 'splendid-plugin', SPLENDID_PLUGIN_URL . 'assets/css/splendid.css', null, $SPLENDID_MAIN_CLASS__CRS_ADDON_VERSION );
	    wp_register_script( 'splendid-plugin', SPLENDID_PLUGIN_URL . 'assets/js/splendid.js', array('jquery'), $SPLENDID_MAIN_CLASS__CRS_ADDON_VERSION );

	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
			return;
		}

		// Check for required Elementor version
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
			return;
		}

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return;
		}

		// Add Plugin actions
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

		//Register Widget Styles
		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'register_scripts' ] );

		// Register Widget Scripts
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'register_scripts' ] );

		// Category
		add_action( 'elementor/elements/categories_registered', array( $this, 'add_elementor_category' ), 5 );
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'wpte-addon' ),
			'<strong>' . esc_html__( 'Elementor Test Extension', 'wpte-addon' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'wpte-addon' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

    /**
     * Add the Category for Theme Widgets.
     */
    function add_elementor_category( $elements_manager ) {

       $elements_manager->add_category(
           'splendid',
           [
               'title' => __( 'Splendid', 'splendid' ),
               'icon' => 'fa fa-plug',
           ]
       );
    }

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'wpte-addon' ),
			'<strong>' . esc_html__( 'Elementor Test Extension', 'wpte-addon' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'wpte-addon' ) . '</strong>',
			 self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'wpte-addon' ),
			'<strong>' . esc_html__( 'Elementor Test Extension', 'wpte-addon' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'wpte-addon' ) . '</strong>',
			 self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Init Widgets
	 *
	 * Include widgets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_widgets() {

		// Include Widget files
		require_once( __DIR__ . '/widgets/hero.php' );
		//require_once( __DIR__ . '/widgets/discover.php' );
		require_once( __DIR__ . '/widgets/testimonial.php' );
		require_once( __DIR__ . '/widgets/goodbye-sec.php' );
		require_once( __DIR__ . '/widgets/wc-product-images.php' );
		require_once( __DIR__ . '/widgets/wc-product-review.php' );

		// Register widget
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SPLENDID_HERO_CLASS() );
		//\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SPLENDID_DISCOVER_CLASS() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SPLENDID_TESTIMONIAL_CLASS() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \GOODBYE_DISCOVER_CLASS() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SPLENDID_Product_Images() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SN_Product_Rating() );

	}

}

SPLENDID_MAIN_CLASS::instance();

/**
 * Register WP Widgets
 *
 */
require_once( __DIR__ . '/wp-widgets/button.php' );
require_once( __DIR__ . '/wp-widgets/cart.php' );

/**
 *  Cart Fragments
 */
function splendid_cart_fragments( $fragments ) {
	global $woocommerce;

    ob_start();
    sn_cart_count();
    $fragments['span.sn-cart-count'] = ob_get_clean();

    /*ob_start();
    wi_checkout_inner();
    $fragments['div.wi-checkout-inner'] = ob_get_clean();*/

    return $fragments;

}
add_filter( 'woocommerce_add_to_cart_fragments', 'splendid_cart_fragments', 10, 1 );

function sn_cart_count(){ ?>
	<span class="sn-cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
	<?php
}