<?php
/**
 * Refer a friend content
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( !is_user_logged_in() ) {
	return;
}

//$affiliate = YITH_WCAF_Affiliate_Handler()->get_affiliate_by_id( 9 );

do_action( 'woocommerce_before_account_my_rewards' ); ?>

<div class="sn-myreward-content">

	<div class="sn-ref-title">
		<h3>My Rewards</h3>
	</div>

	<?php do_action( 'sn_myreward_lists' ); ?>



</div>

<?php do_action( 'woocommerce_after_account_my_rewards' ); ?>
