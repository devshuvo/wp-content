<?php
/**
 * Plugin Name: Splendid Referral
 * Description: Custom Referral Plugin for Splendid
 * Plugin URI:  https://www.splendidnutrition.com/
 * Version:     1.0.0
 * Author:      Shuvo
 * Author URI:  https://www.splendidnutrition.com/
 * Text Domain: splendid
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! defined( 'SPLENDID_REF' ) ) {
	define( 'SPLENDID_REF', true );
}

if ( ! defined( 'SPLENDID_REF_FREE' ) ) {
	define( 'SPLENDID_REF_FREE', true );
}

if ( ! defined( 'SPLENDID_REF_URL' ) ) {
	define( 'SPLENDID_REF_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'SPLENDID_REF_DIR' ) ) {
	define( 'SPLENDID_REF_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'SPLENDID_REF_INC' ) ) {
	define( 'SPLENDID_REF_INC', SPLENDID_REF_DIR . 'includes/' );
}

if ( ! defined( 'SPLENDID_REF_INIT' ) ) {
	define( 'SPLENDID_REF_INIT', plugin_basename( __FILE__ ) );
}

if ( ! defined( 'SPLENDID_REF_FREE_INIT' ) ) {
	define( 'SPLENDID_REF_FREE_INIT', plugin_basename( __FILE__ ) );
}


final class SplendidReferral {

	// Instance
	private static $_instance = null;

	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	public function __construct() {

		add_action( 'plugins_loaded', [ $this, 'init' ] );

	}

	public function init() {
		add_filter( 'woocommerce_account_menu_items', array( $this, 'add_my_account_menu' ), 15 );
		add_action( 'init', array( $this, 'custom_endpoints' ) );

		// so you can use is_wc_endpoint_url( 'refunds-returns' )
		add_filter( 'woocommerce_get_query_vars', array( $this, 'custom_woocommerce_query_vars' ), 0 );

		add_action( 'woocommerce_account_refer-friend_endpoint', array( $this, 'refer_friend_endpoint_content' ) );
		add_action( 'woocommerce_account_my-rewards_endpoint', array( $this, 'my_rewards_endpoint_content' ) );

	}


    public function add_my_account_menu($items) {

        $key = array_search('orders', array_keys($items));

        if($key !== false){

            $items = (array_merge(array_splice($items, 0, $key + 1), array('refer-friend' => __('Refer-A-Friend','splendid')), $items));
            $items = (array_merge(array_splice($items, 0, $key + 2), array('my-rewards' => __('My Rewards','splendid')), $items));

        }

        else{

            $items['refer-friend'] = __('Refer-A-Friend','splendid');
            $items['my-rewards'] = __('My Rewards','splendid');

        }

        return $items;
    }

	function custom_endpoints() {
	    add_rewrite_endpoint( 'refer-friend', EP_ROOT | EP_PAGES );
	    add_rewrite_endpoint( 'my-rewards', EP_ROOT | EP_PAGES );
	}

	function custom_woocommerce_query_vars( $vars ) {
		$vars['refer-friend'] = 'refer-friend';
		$vars['my-rewards'] = 'my-rewards';
		return $vars;
	}

	function refer_friend_endpoint_content() {
	    wc_get_template( 'myaccount/refer-friend.php');
	}

	function my_rewards_endpoint_content() {
	    wc_get_template( 'myaccount/my-rewards.php');
	}


}

SplendidReferral::instance();


require_once( SPLENDID_REF_INC . 'affwp-functions.php' );
require_once( SPLENDID_REF_INC . 'automatically-mark-affwp-referrals-as-paid.php' );
require_once( SPLENDID_REF_INC . 'new-user-discount-by-ref.php' );
require_once( SPLENDID_REF_INC . 'redirections.php' );
require_once( SPLENDID_REF_INC . 'affwp-conditional-actions.php' );
require_once( SPLENDID_REF_INC . 'am_affwp_social_share.php' );

/**
 * Encrypt and decrypt
 *
 * @param string $string string to be encrypted/decrypted
 * @param string $action what to do with this? e for encrypt, d for decrypt
 */
function sn_simple_crypt( $string, $action = 'e' ) {
    // you may change these values to your own
    $secret_key = 'my_simple_secret_key';
    $secret_iv = 'my_simple_secret_iv';

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }

    return $output;
}

// Encrypt
function sn_encrypt( $data = null ){
	return sn_simple_crypt( $data, 'e' );
}

// Decrypt
function sn_decrypt( $data = null ){
	return sn_simple_crypt( $data, 'd' );
}

// Not required Payment mail


/**
 * Check affiliate enabled for user or not
 * @return boolean [description]
 */
function is_affiliate_enabled(){
	$affiliate = affwp_get_affiliate();

	if ( $affiliate && $affiliate->status == 'active' ) {
		return true;
	} else {
		return;
	}
}

/**
 * Get the referral conversion
 */
function sn_rewards_count(){

	$count = 0;

	$affiliate_id = affwp_get_affiliate_id();
	$aff_referrals_count     = affwp_count_referrals( $affiliate_id );

	if ( $aff_referrals_count >2 ) {
		$count = $aff_referrals_count-2;
	}

	return $count;
}

// My Total Rewards popup counter
function sn_total_rewards_count(){
	$count = 0;

	$balance        = (float) get_user_meta( get_current_user_id(), 'affwp_wc_credit_balance', true );
	$cart_coupons   = WC()->cart->get_applied_coupons();
	$coupon_applied = snwc_affwp_check_for_coupon( $cart_coupons );

	// Control checkou notice display
	$show_checkout_notice = apply_filters( 'show_store_credit_myrewards_notice', true );

	// If the user has a credit balance,
	// and has not already generated and applied a coupon code
	if( $balance && !$coupon_applied && $show_checkout_notice ) {
		$count = $count+1;
	}

	// Count up when has discount and free shipping
	if ( sn_user_has_free_shipping() && sn_new_referrer_has_discount() ) {
		$count = $count+1;
	}

	// Count up when has free box
	if ( is_available_free_box() ) {
		$count = $count+1;
	}

	return $count;
}

/**
 * Total AffiliateWP Referral
 */
function sn_rewards_count_progress(){

	$count = 0;

	$affiliate_id = affwp_get_affiliate_id();
	$aff_referrals_count     = affwp_count_referrals( $affiliate_id );

	if ( $aff_referrals_count ) {
		$count = $aff_referrals_count;
	}


	return $count;
}


// Hide default store credit checkout notice
//add_filter( 'show_store_credit_checkout_notice', '__return_false' );


/**
 * Set Cookie Data
 */
function sn_setcookie( $cookie = null, $value = null ){

    $expiry = strtotime('+1 day');

	if ( $cookie && $value ) {
	    setcookie($cookie, $value, $expiry, COOKIEPATH, COOKIE_DOMAIN);
	    return true;
	} else {
		return false;
	}
}

/**
 * Get Cookie Data
 */
function sn_get_cookie( $cookie = null ){
	if ( $cookie && isset( $_COOKIE[$cookie] ) ) {
		return $_COOKIE[$cookie];
	} else {
		return false;
	}
}

/**
 * Sitewide Set Cookie Function
 *
 */
function splendid_setcookie_sitewide(){

	if ( is_admin() ) {
		return;
	}

    $user_id = get_current_user_id();

    $expiry = strtotime('+12 month');

    if( isset( $_GET['new_ref_discount'] ) ) {
    	if ( $_GET['new_ref_discount'] == "true" ) {
    		sn_setcookie( 'new_referrer_claimed', sn_encrypt('1') );
    	} else {
    		sn_setcookie( 'new_referrer_claimed', sn_encrypt('0') );
    	}

        wp_redirect( remove_query_arg( 'new_ref_discount' ) ); exit;
    }

    if( isset( $_GET['dc'] ) ) {
    	sn_delete_cookie($_GET['dc']);
    }



}
add_action('init', 'splendid_setcookie_sitewide', 5 );
//add_action('template_redirect', 'splendid_setcookie_sitewide', 5 );


/**
 * Get Cookie Data
 */
function sn_delete_cookie( $cookie = null ){

    $expiry = strtotime('-1 day');

	if ( $cookie && isset( $_COOKIE[$cookie] ) ) {
	    unset( $_COOKIE[$cookie] );

	    setcookie($cookie, '', $expiry, COOKIEPATH, COOKIE_DOMAIN);
	    setcookie($cookie, '', $expiry, "/");

	    return true;
	} else {
		return false;
	}
}

/**
 * Delete Cookies On logout
 */
function delete_cookies_on_logout_function() {
    // your code
}
//add_action( 'wp_logout', 'delete_cookies_on_logout_function' );

/**
 * Get referrer email
 */
function sn_get_referrer_email(){
	return sn_get_cookie('referrer_email');
}

/**
 * Add a discount to an Orders programmatically
 * (Using the FEE API - A negative fee)
 *
 * @since  3.2.0
 * @param  int     $order_id  The order ID. Required.
 * @param  string  $title  The label name for the discount. Required.
 * @param  mixed   $amount  Fixed amount (float) or percentage based on the subtotal. Required.
 * @param  string  $tax_class  The tax Class. '' by default. Optional.
 */
function wc_order_add_discount( $order_id, $title, $amount, $tax_class = '' ) {
    $order    = wc_get_order($order_id);
    $subtotal = $order->get_subtotal();
    $item     = new WC_Order_Item_Fee();

    if ( strpos($amount, '%') !== false ) {
        $percentage = (float) str_replace( array('%', ' '), array('', ''), $amount );
        $percentage = $percentage > 100 ? -100 : -$percentage;
        $discount   = $percentage * $subtotal / 100;
    } else {
        $discount = (float) str_replace( ' ', '', $amount );
        $discount = $discount > $subtotal ? -$subtotal : -$discount;
    }

    $item->set_tax_class( $tax_class );
    $item->set_name( $title );
    $item->set_amount( $discount );
    $item->set_total( $discount );

    if ( '0' !== $item->get_tax_class() && 'taxable' === $item->get_tax_status() && wc_tax_enabled() ) {
        $tax_for   = array(
            'country'   => $order->get_shipping_country(),
            'state'     => $order->get_shipping_state(),
            'postcode'  => $order->get_shipping_postcode(),
            'city'      => $order->get_shipping_city(),
            'tax_class' => $item->get_tax_class(),
        );
        $tax_rates = WC_Tax::find_rates( $tax_for );
        $taxes     = WC_Tax::calc_tax( $item->get_total(), $tax_rates, false );
        print_pr($taxes);

        if ( method_exists( $item, 'get_subtotal' ) ) {
            $subtotal_taxes = WC_Tax::calc_tax( $item->get_subtotal(), $tax_rates, false );
            $item->set_taxes( array( 'total' => $taxes, 'subtotal' => $subtotal_taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        } else {
            $item->set_taxes( array( 'total' => $taxes ) );
            $item->set_total_tax( array_sum($taxes) );
        }
        $has_taxes = true;
    } else {
        $item->set_taxes( false );
        $has_taxes = false;
    }
    $item->save();

    $order->add_item( $item );
    $order->calculate_totals( $has_taxes );
    $order->save();
}


/**
 * Custom price function
 */
function wc_formatted_price( $price, $args = array() ) {
    extract( apply_filters( 'wc_price_args', wp_parse_args( $args, array(
        'ex_tax_label' => false,
        'currency' => '',
        'decimal_separator' => wc_get_price_decimal_separator(),
        'thousand_separator' => wc_get_price_thousand_separator(),
        'decimals' => wc_get_price_decimals(),
        'price_format' => get_woocommerce_price_format(),
 	) ) ) );

    $negative = $price < 0;
    $price = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * -1 : $price ) );
    $price = apply_filters( 'formatted_woocommerce_price', number_format( $price, $decimals, $decimal_separator, $thousand_separator ), $price, $decimals, $decimal_separator, $thousand_separator );

    if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $decimals > 0 ) {
        $price = wc_trim_zeros( $price );
    }

    $formatted_price = ( $negative ? '-' : '' ) . sprintf( $price_format, get_woocommerce_currency_symbol( $currency ) , $price );


    return $formatted_price;
}



/**
 * More to count - Referral Page
 * /my-account/refer-friend/
 */
function sn_more_to_go_count(){
	$need_steps = 3;
	if ( sn_rewards_count_progress() < $need_steps ) {
		$return = $need_steps - sn_rewards_count_progress();
	} else {
		$return = 0;
	}

	return  $return;
}


/**
 * More to count with text - Referral Page
 * /my-account/refer-friend/
 */
function sn_more_to_go_count_with_text(){

	$ref_count = sn_more_to_go_count();

	if ( $ref_count == 0 ) {

		return "Earn a Free Box: Complete";

	} else {
		return "Progress toward a FREE Box! {$ref_count} more to go!";
	}

	return;
}

/**
 * Steps - Referral Page
 * /my-account/refer-friend/
 */
function sn_referrer_progress_count( $count = null ){
	if ( $count ) {
		$classes = "";

		if ( sn_rewards_count_progress() >= $count ) {
			$classes = " active ";
		}

		return "<li class='{$classes}'>{$count}</li>";
	}

	return;
}

/**
 * Social Share Tags
 */
function sn_opengraph_share_tags(){

	// Return early if not using AffiliateWP.
	if ( ! function_exists( 'affiliate_wp' ) ) {
		return;
	}

	// Get the referral variable being used in AffiliateWP.
	$ref = affiliate_wp()->tracking->get_referral_var();

	// Redirect the user to the location specified, but only if not on the $location page.
	if ( isset( $_GET[$ref] ) ) :

		$og_url = home_url( '/?'.$ref.'='.$_GET[$ref] );
		$og_type = 'website';
		$og_title = apply_filters( 'plugin_text', get_option( 'yith_wcaf_socials_title' ) );
		$og_description =  str_replace( '%referral_url%', $og_url, get_option( 'yith_wcaf_socials_text' ) );
		$og_image = esc_url( get_option( 'yith_wcaf_socials_image_url' ) );
		//$og_image = "https://www.splendidnutrition.com/wp-content/uploads/2020/09/Hero-1.png";

    	?>
		<meta property="og:url"           content="<?php echo $og_url; ?>" />
		<meta property="og:type"          content="<?php echo $og_type; ?>" />
		<meta property="og:title"         content="<?php echo $og_title; ?>" />
		<meta property="og:description"   content="<?php echo $og_description; ?>" />
		<meta property="og:image"         content="<?php echo $og_image; ?>" />
	<?php endif; ?>

	<?php
}
//add_action( 'wp_head', 'sn_opengraph_share_tags', 5 );

/**
 * Add Free Shipping
 */
function sn_add_free_shipping(){

	if ( !is_user_logged_in() ) {
		return;
	}

	$user_id = get_current_user_id();

	//$get_free_shipping = get_user_meta( $user_id, 'has_free_shipping', true) ? get_user_meta( $user_id, 'has_free_shipping', true) : 0;

	//$get_free_shipping = $get_free_shipping+1;

	update_user_meta( $user_id, 'has_free_shipping', 1 );

}

/**
 * Add Free Shipping
 */
function sn_remove_free_shipping(){

	if ( !is_user_logged_in() ) {
		return;
	}

	$user_id = get_current_user_id();

	$get_free_shipping = get_user_meta( $user_id, 'has_free_shipping', true) ? get_user_meta( $user_id, 'has_free_shipping', true) : 0;

	$get_free_shipping = $get_free_shipping-1;

	if ( $add_free_shipping < 0 ) {
		update_user_meta( $user_id, 'has_free_shipping', "0" );
	} else {
		update_user_meta( $user_id, 'has_free_shipping', $get_free_shipping );
	}

}

function snwc_over_amount_free_shipping(){
	return 29.92;
}

/**
 * Placed order hooks
 */
//add_action( 'woocommerce_order_status_processing', 'snwc_placed_order', 10, 1 );
//add_action( 'woocommerce_order_status_completed', 'snwc_placed_order', 10, 1 );
add_action( 'woocommerce_thankyou', 'snwc_placed_order_action', 30, 1 );
function snwc_placed_order_action( $order_id ){

	// Delete cookie for logged out user
	sn_delete_cookie('new_referrer');
	sn_delete_cookie('new_referrer_claimed');
	sn_delete_cookie('affwp_ref');
	sn_delete_cookie('affwp_ref_visit_id');

	// Get an instance of WC_Order object
	$order = wc_get_order( $order_id );
	$user_id = $order->get_customer_id();

	// FreeBox Flag
	$had_free_box = 0;

	// Coupons used in the order LOOP (as they can be multiple)
	foreach( $order->get_used_coupons() as $coupon_code ){

	    // Retrieving the coupon ID
	    $coupon_post_obj = get_page_by_title($coupon_code, OBJECT, 'shop_coupon');
	    $coupon_id       = $coupon_post_obj->ID;

	    // Get coupon type
		$sn_coupon_type = get_post_meta( $coupon_id, 'sn_coupon_type', ture );

	    // Get an instance of WC_Coupon object in an array(necessary to use WC_Coupon methods)
	    //$coupon = new WC_Coupon($coupon_id);

	    // Or use this other conditional method for coupon type
	    if( $sn_coupon_type == 'freebox_freeshipping' ){
	    	// Add flag counter
	        $had_free_box = $had_free_box+1;
	    }
	}


	if ( $had_free_box  ) {

	    update_user_meta( $user_id, 'has_free_box', 'no' );
	    update_user_meta( $user_id, 'free_box_purchased', '1' );

	} else {

		update_user_meta( $user_id, 'new_referrer', 0 );
		update_user_meta( $user_id, 'new_referrer_claimed', 1 );

		// If Subtotal is not included free shipping
		//if ( $order->get_subtotal() < snwc_over_amount_free_shipping() ) {
			update_user_meta( $user_id, 'has_free_shipping', 0 );
		//}

	}

}


function remove_class_action($tag, $class = '', $method, $priority = null) : bool {
    global $wp_filter;
    if (isset($wp_filter[$tag])) {
        $len = strlen($method);

        foreach($wp_filter[$tag] as $_priority => $actions) {

            if ($actions) {
                foreach($actions as $function_key => $data) {

                    if ($data) {
                        if (substr($function_key, -$len) == $method) {

                            if ($class !== '') {
                                $_class = '';
                                if (is_string($data['function'][0])) {
                                    $_class = $data['function'][0];
                                }
                                elseif (is_object($data['function'][0])) {
                                    $_class = get_class($data['function'][0]);
                                }
                                else {
                                    return false;
                                }

                                if ($_class !== '' && $_class == $class) {
                                    if (is_numeric($priority)) {
                                        if ($_priority == $priority) {
                                            //if (isset( $wp_filter->callbacks[$_priority][$function_key])) {}
                                            return $wp_filter[$tag]->remove_filter($tag, $function_key, $_priority);
                                        }
                                    }
                                    else {
                                        return $wp_filter[$tag]->remove_filter($tag, $function_key, $_priority);
                                    }
                                }
                            }
                            else {
                                if (is_numeric($priority)) {
                                    if ($_priority == $priority) {
                                        return $wp_filter[$tag]->remove_filter($tag, $function_key, $_priority);
                                    }
                                }
                                else {
                                    return $wp_filter[$tag]->remove_filter($tag, $function_key, $_priority);
                                }
                            }

                        }
                    }
                }
            }
        }

    }

    return false;
}

//add_action('woocommerce_before_checkout_form', function() {
//    remove_class_action('woocommerce_before_checkout_form', 'AffiliateWP_Store_Credit_WooCommerce', 'action_add_checkout_notice', 10);
//}, 5);

// Social shortcode for offer details page
function easy_social_shortcode_function( $atts ){

	if ( function_exists('get_field') ) {
		$social_share_shortcode = get_field('easy_social_shortcode','option');
		return "<div class='easy_social_shortcode'>".do_shortcode( $social_share_shortcode )."</div>";
	}

}
add_shortcode( 'easy_social_shortcode', 'easy_social_shortcode_function' );

