<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$for_what_text = get_field('for_what_text') ? get_field('for_what_text') : "";
$free_shipping_tag_text = get_field('free_shipping_tag_text') ? get_field('free_shipping_tag_text') : null;

$serving_info_text = get_field('serving_info_text') ? get_field('serving_info_text') : null;
$subscribe_save_value = get_field('subscribe_save_value') ? get_field('subscribe_save_value') : null;

$serving_price_text = get_field('serving_price_text') ? get_field('serving_price_text') : null;

$subscribesave_serving_info_text = get_field('subscribesave_serving_info_text') ? "(Subscribe & save $".$subscribe_save_value." <br>".get_field('subscribesave_serving_info_text').")" : null;



?>
<li data-id="<?php esc_attr_e( get_the_ID() );?>" <?php wc_product_class( ' sn-find-focus-row ', $product ); ?>>

	<?php if( $free_shipping_tag_text ) : ?>
	<span class="free_shipping_tag hide_on_grid">
		<?php echo $free_shipping_tag_text; ?>
	</span>
	<?php endif; ?>

	<div class="snff-title-wrap hide_on_grid">
		<?php woocommerce_template_loop_product_title(); ?>
		<?php do_action( 'sn_after_product_title', $product ); ?>

		<div class="ssff-text">
			<div class="text-sm"><?php echo $for_what_text; ?></div>
		</div>
	</div>

	<div class="snff-thumbnail text-center">
		<a href="<?php echo get_permalink( get_the_ID() ); ?>" class="snff-product-link">
			<?php woocommerce_template_loop_product_thumbnail(); ?>
		</a>
	</div>

	<div class="ssff-serving text-center hide_on_grid">
		<?php do_action( 'sn_product_serving', $product ); ?>

		<div class="ssff-text">
			<p><?php echo $serving_info_text; ?></p>
			<div class="text-sm hide-on-phone"><?php echo $serving_price_text; ?></div>
			<p class="show-on-phone ssff-price-mobile"><?php woocommerce_template_loop_price(); ?></p>
		</div>
	</div>

	<div class="ssff-price text-center hide_on_grid">
		<?php do_action( 'sn_product_price', $product ); ?>
		<div class="ssff-text">
			<?php woocommerce_template_loop_price(); ?>
			<div class="text-sm"><?php echo $subscribesave_serving_info_text; ?></div>
		</div>
	</div>

	<div class="ssff-cart-popup hide_on_grid">
		<a href="javascript:void(0)" class="ssff-cart-popup-btn" data-product_id="<?php echo $product->get_id(); ?>"><img src="<?php echo SPLENDID_IMG_DIR;?>/cart-popup.svg"></a>
	</div>

	<div class="ssff-grid-layout text-center">

		<div class="ssff-grid-title">
			<h5><?php echo get_the_title(); ?></h5>
		</div>

		<div class="ssff-grid-price">
			<?php woocommerce_template_loop_price(); ?>
		</div>

		<div class="ssff-grid-rating">
			<?php woocommerce_template_loop_rating(); ?>

		</div>
		<div class="ssff-grid-add-to-cart">
			<?php woocommerce_template_loop_add_to_cart(); ?>
		</div>
	</div>
	<?php

	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>
