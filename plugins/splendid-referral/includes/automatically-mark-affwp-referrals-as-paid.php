<?php

// query to redem -- disabled now
function affwp_redeem_unpaid_referrals(){

	if( isset( $_GET['affwp_wc_redeem_credit'] ) && $_GET['affwp_wc_redeem_credit'] ) {

		$affiliate_id = affwp_get_affiliate_id();

		/** @var \AffWP\Referral[] $referrals */
		$referrals = affiliate_wp()->referrals->get_referrals(
			array(
				'affiliate_id' => $affiliate_id,
				'status'       => array( 'unpaid' ),
			)
		);

		if ( $referrals ) {
			foreach ( $referrals as $referral ){
				affwp_set_referral_status( $referral->referral_id, 'paid' );

				//$referral = affwp_get_referral( $id );
				//if ( $referral && current_user_can( 'manage_payouts' ) ) {
				//	affwp_add_payout( array(
				//		'affiliate_id'  => $referral->affiliate_id,
				//		'referrals'     => $id,
				//		'payout_method' => 'manual'
				//	) );
				//}

			}

			wp_redirect( remove_query_arg( 'affwp_wc_redeem_credit' ) ); exit;
		}

	}

}
//add_action( 'init', 'affwp_redeem_unpaid_referrals' );


/**
 * Register a new WP Cron schedule of 180 seconds.
 *
 * @see http://codex.wordpress.org/Plugin_API/Filter_Reference/cron_schedules
 */
function isa_add_every_three_minutes( $schedules ) {
    $schedules['every_three_minutes'] = array(
        'interval' => 15,
        'display' => __( 'Every 15 Sec', 'textdomain' )
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'isa_add_every_three_minutes' );

/**
 * Schedule cron job to mark the last 50 AffiliateWP referrals as paid.
 */
function affwp_rcp_schedule_mark_referrals_as_paid_cron() {
    $is_event_scheduled = (bool) wp_next_scheduled( 'affwp_rcp_mark_referrals_as_paid' );

    if ( ! $is_event_scheduled ) {
        wp_schedule_event( time(), 'every_three_minutes', 'affwp_rcp_mark_referrals_as_paid' );
    }
}
//add_action( 'init', 'affwp_rcp_schedule_mark_referrals_as_paid_cron' );

/**
 * Automatically mark the last 50 Affiliate WP referrals as paid.
 */
function affwp_rcp_automatically_mark_referrals_as_paid() {
    if ( ! class_exists( 'Affiliate_WP_Referrals_DB' ) ) {
        return;
    }

    $referrals_db = new Affiliate_WP_Referrals_DB;

    $affiliates = $referrals_db->get_referrals( array(
        'number'       => 50,
        'offset'       => 0,
        'referrals_id' => 0,
        'affiliate_id' => 0,
        'reference'    => '',
        'context'      => '',
        'status'       => '',
        'orderby'      => 'referral_id',
        'order'        => 'DESC',
        'search'       => false
    ) );

    foreach ( $affiliates as $affiliate ) {
        if ( 'pending' == $affiliate->status || 'unpaid' == $affiliate->status ) {
            affwp_set_referral_status( $affiliate->referral_id, "paid" );
        }
    }

    wp_clear_scheduled_hook( 'affwp_rcp_mark_referrals_as_paid' );
}
add_action( 'affwp_rcp_mark_referrals_as_paid', 'affwp_rcp_automatically_mark_referrals_as_paid' );

//add_action( 'affwp_insert_affiliate', 'affwp_rcp_automatically_mark_referrals_as_paid' );
//add_action( 'woocommerce_thankyou', 'affwp_rcp_automatically_mark_referrals_as_paid', 10, 1 );
//add_action( 'affwp_rcp_mark_referrals_as_paid', 'affwp_rcp_automatically_mark_referrals_as_paid' );