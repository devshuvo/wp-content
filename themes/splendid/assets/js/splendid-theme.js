// Get Cookie function
function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}

function setCookie(cname, cvalue, exdays = 30) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// Start Jquery
(function($){
	'use strict'
	$(document).ready(function($){
		console.log('hello from theme');

		$(document).on('click', '.direct_checkout_btn, .direct_cart_btn', function(e){

			var qty = $(this).closest('form').find('input.qty').val();

			$(this).attr("href", function() {
				return this.href + '&quantity=' + qty;
			});


			//e.preventDefault();
		});

		// Direct cart button
		$(document).on('click', '.direct_cart_btn', function(e){
			$(this).closest('form').find('button[type="submit"]').click();
		});

		// Share Icon click
		$(document).on('click', '.snref-share-wrap .snref-share-svg', function(){

			console.log('clicked');
			$(this).closest('li').find('#share_link_button').click();

		});

		// Popup Close overlay
		$(document).on('click', '.close-popup-overlay', function(){

			var popUpId = $(this).attr('data-id');

			$('#'+popUpId).find('.trigger_close_btn').click();

		});


		// TOC
		$(document).on('mouseover', '.sn-sticky-toc .elementor-toc__list-item-text', function(e){

			// target element id
    		var id = $(this).attr('href');
    		if ( id ) {
	    		$(this).attr('href','');
	    		$(this).attr('data-href', id);
	    	}

		});

		$(document).on('click', '.sn-sticky-toc .elementor-toc__list-item-text', function(e){

			e.preventDefault();

			// target element id
    		var id = $(this).attr('href');
    		if ( id ) {
	    		$(this).attr('href','');
	    		$(this).attr('data-href', id);
	    	}

    		var data_id = $(this).attr('data-href');

		    // target element
		    var $data_id = $(data_id);
		    if ($data_id.length === 0) {
		        return;
		    }

		    if ( $(window).width() > 767 ) {
		    	var fixTop = 160;
		    } else {
		    	var fixTop = 260;
		    }

			// top position relative to the document
		    var pos = $data_id.offset().top-fixTop;

		    // animated top scrolling
		    $('body, html').animate({scrollTop: pos});

		    console.log(fixTop,pos);

			return false;

		});

		// Skip Create an account on Checkout
		$(document).on('click', '.skip_from_signup', function(e){

			$(document).find('label[for="sn_createaccount"]').click();

		});

		// Custom Radio Click to trigger
		$(document).on('click', '.sn_checkbox-icon, .sn_radio-icon', function(e){

			//$(this).parent().find('label').click();
			$(this).siblings('label').click();

			if ( $(this).parent().hasClass('wc_payment_method') ) {
				$(this).siblings('.input-radio').click().prop( 'checked', true );
			}

		});

		// WC comment button
		$(document).on('click', '.add-review-btn', function(e){
			e.preventDefault();
			$("#review_form_wrapper").slideToggle();
		});

		// WC order summary button
		$(document).on('click', '.order-summary-toggle', function(e){
			$(this).toggleClass('active');
			$(".sn-checkout-right").slideToggle('fast');
		});

		$(document).on('click', '.trigger_back_btn, .trigger_close_btn', function(e){
			e.preventDefault();

			$(this).closest('.sn_popup_container, .sns-popup').fadeOut();

			var openedPopup = $('html').attr('data-opened-popup');

			if ( openedPopup > 0 ) {
				openedPopup = parseInt(openedPopup)-1;
			} else {
				//openedPopup = 0;
			}

			$('html').attr('data-opened-popup', openedPopup);

			if ( openedPopup < 1 ) {
				$('html').removeClass('popup-active');
			}

			var openedPopupId = $(this).closest('.sn_popup_container, .sns-popup').attr('id');

			$('.modal_show').each(function(i, el){
				if( $(this).attr('data-modal_id') == openedPopupId ){
					$(this).removeClass('modal_show');
				}
			});

		});


		$(document).on('click', '[data-modal_id]:not(.modal_show)', function(e){
			e.preventDefault();

			$(this).addClass('modal_show');

			var modal_id = $(this).data('modal_id');

			$(document).find('#'+modal_id).fadeIn();

			var openedPopup = $('html').attr('data-opened-popup');

			if ( openedPopup ) {
				openedPopup = parseInt(openedPopup)+1;
			} else {
				//openedPopup = 0;
			}

			$('html').addClass('popup-active').attr('data-opened-popup', openedPopup);

		});

		// form toggle
		$(document).on('click', '[data-toggle-show]', function(e){
			var tId = $(this).data('toggle-show');

			$(this).closest('[data-toggle-hide]').hide();

			$(tId).show();

		});

		// Mobile Buy Now trigger
		$(document).on('click', '.buy-now-link a', function(e){
			e.preventDefault();
			jQuery('.ast-nav-close').click();
			$('[data-modal_id="buy_now_trigger"]').trigger('click');
		});

		// Purchase Type Changing
		$(document).on('change', '.radio-container input[name="purchase_type"]', function(e){
			var tVal = $(this).val();

			if (tVal == 1) {
				$(document).find('.show_on_sns').addClass('show');
				$(document).find('.show_on_otp').removeClass('show');
			} else {
				$(document).find('.show_on_sns').removeClass('show');
				$(document).find('.show_on_otp').addClass('show');
			}

		});

		// Price Change on qty change
		jQuery(document).on('change', 'input[name="quantity"]', function(e){
			var $this = jQuery(this);
			var qty = $this.val();

			// .snpopup-price
			jQuery('.sn-product-price .sn_wc_price, .snpopup-price .sn_wc_price').each(function(el,i ){
				var thePrice = parseFloat( jQuery(this).data('price') );

				var returnPrice = (thePrice*qty).toFixed(2);

				jQuery(this).text( returnPrice );

			});

		});

		// Different shippiing address Changing
		$(document).on('change', '.sn_different_shipping', function(e){
			var tVal = $(this).val();

			$('.shipping_address').removeClass('hidden').hide();

			if (tVal == 1) {
				//jQuery('#ship-to-different-address-checkbox').prop('checked', 1).change();
				$('.shipping_address').slideDown();
			} else {
				//jQuery('#ship-to-different-address-checkbox').prop('checked', 0).change();

				$('.shipping_address').slideUp();
				$(document).trigger('sync_billing_to_shipping');
			}

			//console.log(tVal);

		});

		// Create Checkout Account Checkbox
		$(document).on('change', '#sn_createaccount', function(e){
			var tVal = $(this).is(':checked');

			if (tVal) {
				jQuery('.create-skip-pass').slideDown();
				jQuery('.create-account label.woocommerce-form__label').trigger('click');
			} else {
				jQuery('.create-skip-pass').slideUp();
				jQuery('.create-account label.woocommerce-form__label').trigger('click');
			}

		});

		// Checkbox add class
		$(document).on('change', '.sn_radio-wrap input[type="radio"]', function(e){
			var tVal = $(this).val();
			var tChecked = $(this).is(':checked');

			$('.sn_radio-wrap').removeClass('active');

			if (tChecked) {
				$(this).closest('.sn_radio-wrap').addClass('active');
				jQuery('#shipping_country').change();
			}

		});

		// Payment Checkbox add class
		$(document).on('change', '.wc_payment_method input[type="radio"]', function(e){
			var tVal = $(this).val();
			var tChecked = $(this).is(':checked');

			$('.wc_payment_method').removeClass('active');

			if (tChecked) {
				$(this).closest('.wc_payment_method').addClass('active');
			}

		});

		function sn_billing_validated(){
			var flag = 0;

			jQuery('.woocommerce-billing-fields .validate-required, .woocommerce-billing-fields .validate-postcode, .sn_validation-required').each(function(i,el){

				var inputVal = jQuery(this).find('input, select').val();

				if ( inputVal == "" || inputVal == "default" ) {
					jQuery(this).addClass('woocommerce-invalid');
					flag++;
				} else {
					jQuery(this).removeClass('woocommerce-invalid');
				}

			});


			if( flag > 0 ){
				return false;
			} else {
				return true;
			}
		}

		// Checkout Step Change - to payment
		$(document).on('click', '.process-to-payment, .step-payment', function(e) {
			e.preventDefault();

			if ( sn_billing_validated() ) {
				$('.show_on_ci').hide();
				$('.step-payment').removeClass('inactive');

				$('.show_on_pi').show();

				$(document).trigger('show_submitted_shipping_info');

				$('.sn_notifications').html('');

			} else {
				$('.sn_notifications').html('<span class="sn-danger">Whoops! Please enter required fields.</span>');
			}

			// Change Billing Address/Payment order
        	jQuery('.woocommerce-shipping-fields-wrap').insertAfter('.woocommerce-checkout-review-order-wrap');

			$("html, body").animate({ scrollTop: 0 }, 300);
 		 	return false;

		});

		function force_payment_tab(){
			$('.show_on_ci').hide();
			$('.step-payment').removeClass('inactive');
			$('.show_on_pi').show();
			$('.sn_notifications').html('');
			$("html, body").animate({ scrollTop: 0 }, 300);
 		 	return false;
		}

		$(document).on('force_payment_tab', function(){
			force_payment_tab();
		});

		// Checkout Step Change - to customer info
		$(document).on('click', '.step-information, .edit-billing-info', function(e) {
			e.preventDefault();

			$('.show_on_ci').show();
			$('.step-payment').addClass('inactive');

			$('.show_on_pi').hide();

			$(document).trigger('show_submitted_shipping_info');

			$("html, body").animate({ scrollTop: 0 }, 300);
 		 	return false;

		});
		// End Checkout Step Change

		// Amazon Pay Trigger
		$(document).on('click', '.sn-amazon-pay', function(e) {
			e.preventDefault();

			jQuery('#pay_with_amazon img').click();

		});


		// Sync Shipping to billing info
		$(document).on('change', '.woocommerce-billing-fields__field-wrapper input, .woocommerce-billing-fields__field-wrapper select', function(e){
			$(document).trigger('sync_billing_to_shipping');

			/*var value = $(this).val();
			var element = $(this).attr('id');

			var bElement = element.replace("shipping", "billing");

			if ( element == "shipping_state" ) {
				$('#billing_state').val(value).change();
			} else if ( element == "shipping_country" ) {
				$('#billing_country').val(value).change();
			} else if ( element == "shipping_postcode" ) {
				$('#billing_postcode').val(value).change();
			} else {
				if ( $.trim( $("#"+bElement).val() ) == "" ) {
					$('#'+bElement).val(value).trigger('change');
				}
			}*/

		});

		$(document).on('sync_billing_to_shipping', function() {

			// Shipping fields value
			var shipping_first_name = jQuery('#shipping_first_name').val();
			var shipping_last_name  = jQuery('#shipping_last_name').val();
			var shipping_address_1  = jQuery('#shipping_address_1').val();
			var shipping_address_2  = jQuery('#shipping_address_2').val();
			var shipping_city  = jQuery('#shipping_city').val();
			var shipping_state  = jQuery('#shipping_state ').val();
			var shipping_country   = jQuery('#shipping_country').val();
			var shipping_postcode   = jQuery('#shipping_postcode').val();
			var shipping_phone   = jQuery('#shipping_phone').val();

			if ( jQuery('.sn_different_shipping:checked').val() != 1 ) {

				$('#billing_first_name').val(shipping_first_name).trigger('change');
				$('#billing_last_name').val(shipping_last_name).trigger('change');
				$('#billing_address_1').val(shipping_address_1).trigger('change');
				$('#billing_address_2').val(shipping_address_2).trigger('change');
				$('#billing_city').val(shipping_city).trigger('change');
				$('#billing_state').val(shipping_state).trigger('change');
				$('#billing_country').val(shipping_country).trigger('change');
				$('#billing_postcode').val(shipping_postcode).trigger('change');
				$('#billing_phone').val(shipping_phone).trigger('change');
			}



		});

		$(document).on('show_submitted_shipping_info', function() {
			var formData = $('form.checkout').serializeArray();

			formData.forEach(function(el, i){
				//console.log( el.name, el.value );

				var value = el.value;
				var element = el.name;

				if (value) {
					if ( element == "shipping_city" || element == "shipping_state" ) {
						$('.sn-'+element).text(value+',').show();
					} else if( element == "billing_email" ) {
						$('.sn-'+element).text(value).show();
					} else {
						$('.sn-'+element).text(value).show();
					}

				} else {
					$('.sn-'+element).hide();
				}

			});

		});
		// End Show customer billing info


		// Complete order form submit
		$(document).on('click', '.sn-complete-order', function() {
			$('button#place_order').trigger('click');
		});

		// Skip login form
		$(document).on('click', '.skip_from_login', function() {
			$(this).closest('.woocommerce-form-login').slideUp();
		});

		// Coupon Code Applying
		$(document).on('click', '#sn_coupon_submit', function() {
			$(this).closest('.sn-form-coupon').block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
			$('.checkout_coupon').submit();
		});

		$( document.body ).on('applied_coupon_in_checkout', function() {
			jQuery('.sn-form-coupon').unblock();
		});

		$(document).on('change keyup', '#sn_coupon_code', function() {
			var thisVal = $(this).val();
		    $('#coupon_code').val(thisVal);
		});

		$(document).on('change keyup', '#coupon_code', function() {
			var thisVal = $(this).val();
		    $('#sn_coupon_code').val(thisVal);
		});
		// End Coupon Code Applying

		// Email Sync for checkout
		$(document).on('change keyup', '#sn_billing_email', function() {
		    $('#billing_email').val($(this).val());
		    console.log('sn billing email change');
		});
		$(document).on('change keyup', '#billing_email', function() {
		    $('#sn_billing_email').val($(this).val());
		    console.log('billing email change');
		});
		//jQuery("#billing_email").trigger('change');

		// Password Sync for checkout
		$(document).on('change keyup', '#sn_checkout_signup #password', function() {
		    $('#account_password').val($(this).val());
		});
		$(document).on('change keyup', '#account_password', function() {
		    $('#sn_checkout_signup #password').val($(this).val());
		});

		// Password Sync for AffiliateWp
		$(document).on('change keyup', '#affwp-user-pass', function() {
		    $('#affwp-user-pass2').val($(this).val());
		});
		$(document).on('change keyup', '#affwp-user-pass2', function() {
		    $('#affwp-user-pass').val($(this).val());
		});

		// Popup Cart button
		$(document).on('click', '.ssff-cart-popup-btn', function(e){
			e.preventDefault();

		    var $this = $(this);

		    var product_id = $(this).data('product_id');

		    var data = {
		        action: 'sn_product_detail_popup',
		        nonce: sn_ajax.nonce,
		        product_id: product_id,
		    }

		    $.ajax({
		      url: sn_ajax.url,
		      type: 'post',
		      data: data,
		      beforeSend : function ( xhr ) {
		        $this.addClass('disabled');
		        $('.sn_popup_container').find('.sn-product-wrap-outer').remove();
		        $('.sn_popup_container').fadeIn();
		        $('.splendid_logo_loader').fadeIn();


				var openedPopup = $('html').attr('data-opened-popup');
				if ( openedPopup ) {
					openedPopup = parseInt(openedPopup)+1;
				} else {
					//openedPopup = 0;
				}
				$('html').addClass('popup-active').attr('data-opened-popup', openedPopup);

		      },
		      success: function( res ) {
		      	$('.splendid_logo_loader').hide();
		      	$this.removeClass('disabled');

		      	// Data push
		      	$('.sn_popup_container').append(res);
		      },
		      error: function( result ) {
		      	//$('html').addClass('popup-active');
		        console.error( result );
		      }
		    });

		});

		// Perform AJAX login on form submit
        $(document).on('submit', 'form#login', function(e){
            e.preventDefault();
            var $this = $(this);

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: sn_ajax.url,
                data: {
                    'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                    'username': $('form#login #username').val(),
                    'password': $('form#login #password').val(),
                    'security': $('form#login #security').val()
                },
                success: function(data){
                    if ( data.loggedin == true ){
                        jQuery(document).trigger('ref_step_next');
                    } else {
                    	$this.find('.status').html( data.message );
                    }
                },
                beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			error: function(data){
    				console.log(data);
    			},
            });

        });

        // Register Form - Referrel
		$(document).on('submit', 'form#signup', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		// handle errors on survey page
    		if ( $('.survey-page').hasClass('survey-error-msg') ) {
    			if ( $('body').hasClass('survey_completed') ) {

    			} else {
    				$('body').addClass('survey_error');

    				$("html, body").animate({ scrollTop: 0 }, 300);
    				return;
    			}
    		}

    		var formData = new FormData(this);
            formData.append('action', 'sn_register_form');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('ref_step_next');
                    } else {
                    	$this.find('.status').html( response.error_log );
                    }

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

		// Register Form - Checkout
		$(document).on('submit', 'form#sn_checkout_signup', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		var formData = new FormData(this);

            formData.append('newsletter_subscribe', jQuery('#sn_checkout-newsletter').is(':checked'));
            formData.append('action', 'sn_register_form__checkout');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('sn_register_form__checkout_success');
                    }

                    $this.find('.status').html( response.message );

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

        // Email Capture
		$(document).on('submit', '#splendid_email_capture', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		var formData = new FormData(this);
            formData.append('action', 'splendid_email_capture');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });

                    $this.find('.notice').html("").hide();
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('ref_step_next');
                    }

                    $this.find('.notice').html( response.notice ).show();

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});


        // Survey Form Submit
		$(document).on('submit', '#customer_survey_form', function(e){
		    e.preventDefault();

    		var $this = $(this);
    		var formData = $this.serializeArray();

    		// populate to wpcf7-form
    		formData.forEach(function(el,i){
    			$(document).find('.wpcf7-form [name="'+el.name+'"]').val(el.value);
    		});

    		$(document).find('.wpcf7-form #cscf7_submit').click();

    		return;
    		// we don't need custom code below

    		var formData = new FormData(this);
    		formData.append('action', 'customer_survey_form');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });

                    $this.find('.notice').html("").hide();
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                        jQuery(document).trigger('ref_step_next');
                    }

                    $this.find('.notice').html( response.notice ).show();

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

		// trigger after survey completed
		$(document).on('customer_survey_submitted', function(){
			//setCookie('is_user_done_survey', 'bFZHcDF0alhzWTYvZGtYZTc1ZyswUT09');
		});


        // Reward Redeem Form Submit
		$(document).on('submit', 'form.reward-redeem', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		var formData = new FormData(this);
            formData.append('action', 'reward_redeem_form');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });

                    $this.find('.reward-notice').html("").hide();
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}


						$this.find('.reward-submit').addClass('reward-submit-applied').html( 'Applied' );

                    }

                    $this.find('.reward-notice').html( response.notice ).show();

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

		// reward apply
		$(document).on('click', '.reward-apply-radio', function(){

			var dataVal = $(this).attr('data-val');
			$(this).closest('form').find('input[name="sn_apply_credit"]').val( dataVal );

			$(this).closest('form').find('.sn_radio-wrap').removeClass('checked');
			$(this).closest('.sn_radio-wrap').addClass('checked');

			if ( dataVal == 'apply_all' ) {
				$(this).closest('form').find('input[name="reward-amount"]').val( sn_ajax.available_discount_amount );
				//$(this).closest('form').submit();
			} else {
				//$(this).closest('form').find('input[name="reward-amount"]').val("");
				$(this).closest('form').find('input[name="reward-amount"]').focus();
			}

		});

		// Reward Apply on checkout Form Submit
		$(document).on('submit', '#apply_credit_form', function(e){
		    e.preventDefault();

    		var $this = $(this);

    		var formData = new FormData(this);
            formData.append('action', 'apply_credit_form');

    		$.ajax({
    			type: 'post',
    			url: sn_ajax.url,
    			data: formData,
    			processData: false,
                contentType: false,
    			beforeSend: function(data){
    				$this.block({
                        message: null,
                        overlayCSS: {
                            background: "#fff",
                            opacity: .5
                        }
                    });

                    $this.find('.reward-notice').html("").hide();
    			},
    			complete: function(data){
    				$this.unblock();
    			},
    			success: function(data){

    				var response = JSON.parse(data);

                    if( response.status == 'success' ) {

                    	jQuery(document.body).trigger('update_checkout');

                    	if ( response.redirect_to ) {
							window.location.replace( response.redirect_to );
						}

                    }

                    $this.find('.reward-notice').html( response.notice ).show();

                    //console.log( response );

    			},
    			error: function(data){
    				console.log(data);
    			},

    		});

		});

		// Share copy
        $('button#share_link_button').click(function(){

            $(this).addClass('copied');
            setTimeout(function(){ $('button#share_link_button').removeClass('copied'); }, 3000);
            $(this).parent().find("#share_link_input").select();
            document.execCommand("copy");
        });

        // Pusle 15sec
        function animate_pulsing(){
        	$('.hero_shape_3').addClass('animating');
	        setTimeout(function(){
	        	$('.hero_shape_3').removeClass('animating');
	        },4000);
        }

        animate_pulsing();

        setInterval(function(){
        	animate_pulsing();
        },15000);


        // checkout total Price change for mobile
        $( document.body ).on( 'updated_checkout', function(e,data){
        	var thePrice = $('.order-total span.woocommerce-Price-amount.amount', data.fragments[".woocommerce-checkout-review-order-table"]).html();
        	jQuery('.ost-total-area').html( thePrice );

        });

        // Referral zero amount to text
        $('td.referral-amount').each(function(){
			// target element id
			var text = $(this).text();

			if( text == "$0.00" ){
				$(this).text("-");
			}


		});

        $(window).load(function(){

        	// My reward count
        	$('.woocommerce-MyAccount-navigation-link--my-rewards a').append('<div class="count">'+sn_ajax.get_rewards+'</div>');

        	// Init popup count
        	$('html').attr('data-opened-popup', "0");

        	// State Trigger
        	jQuery('#shipping_state, #billing_state').trigger('change');


        	// AffWP placeholder
        	$(document).find('#affwp-user-email').attr('placeholder', 'Email');
        	$(document).find('#affwp-user-pass').attr('placeholder', 'Password');


        	jQuery('#billing_state_field, #shipping_state_field, #account_password_field').removeClass('woocommerce-invalid');


			// Subscribe & Save TOS
			$('.purchase_type_class').each(function(){
				if ( $(this).hasClass('subscribe-save') ) {
					$('.subscrive-save-tos').show();
				}
			});

			$(document).on('keyup', '#affwp-user-email', function(){
				var email = $(this).val();
	        	if ( email ) {

					var name   = email.substring(0, email.lastIndexOf("@"));
		        	$('.sn-form-area').find('#affwp-user-name').val( name );
		        	$('.sn-form-area').find('#affwp-user-login').val( name );

	        	}
			});

			// Extrate name from email
        	var email = getCookie('referrer_email');
        	if ( email ) {

				var name   = email.substring(0, email.lastIndexOf("@"));
	        	$('.sn-form-area').find('#affwp-user-name').val( name );
	        	$('.sn-form-area').find('#affwp-user-login').val( name );

        	}

			// Auto Populate Referrer Email & name
	        $('.sn-form-area').find('#sn_signup-email, #affwp-user-email').val( getCookie('referrer_email') );
	        $('.splendid_email_capture').find('#email').val( getCookie('referrer_email') );

	        $('form#splendid_email_capture').find('#email').val( getCookie('referrer_email') );


	        // Billing Email trigger
	        if ( jQuery('#billing_email').val() ) {

	        	jQuery('#billing_email').trigger('change');

	        } else if ( getCookie('referrer_email')  ) {

	        	$(document).find('#sn_billing_email').val( getCookie('referrer_email') ).trigger('change');

	        }

	        // TOC Data
	        $(document).on('toc_remove_href', function(){
	        	$('.sn-sticky-toc .elementor-toc__list-item-text').each(function(){
					// target element id
		    		var id = $(this).attr('href');
		    		if ( id ) {
			    		$(this).attr('href','');
			    		$(this).attr('data-href', id);
			    	}
				});
	        });

			if ( $(window).width() < 767 ) {
				jQuery(document).trigger('toc_remove_href');
			}


        });


	});


	$(document).ready(function() {

		$('.woocommerce-product-gallery__image a').each(function(i,el){
			//console.log( $(this).html() );
			$('.woocommerce-product-thumb__wrapper').append('<div class="item-thumb">'+$(this).html()+'</div>');
		});
	});

	// PRODUCT THUMB CAROUSEL OWL SYNC
	$(document).ready(function() {

	  	var sync1 = $("#sync1");
	  	var sync2 = $("#sync2");
	  	var slidesPerPage = 4; //globaly define number of elements per page
	  	var syncedSecondary = true;

	  	sync1.owlCarousel({
	  	  	items : 1,
	  	  	slideSpeed : 2000,
	  	  	nav: true,
	  	  	autoplay:true,
		  	autoplayTimeout:7000,
		  	autoplayHoverPause:true,
	  	  	dots: false,
	  	  	loop: 1,
	  	  	responsiveRefreshRate : 200,
	  	  	navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #999999;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #999999;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
	  	}).on('changed.owl.carousel', syncPosition);

	  	sync2
	  		.on('initialized.owl.carousel', function () {
	  		  	sync2.find(".owl-item").eq(0).addClass("current");
	  		})
	  		.owlCarousel({
	  		items : slidesPerPage,
	  		dots: false,
	  		nav: false,
	  		smartSpeed: 200,
	  		slideSpeed : 500,
	  		slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
	  		responsiveRefreshRate : 100
	  	}).on('changed.owl.carousel', syncPosition2);

	  	function syncPosition(el) {
	  		//if you set loop to false, you have to restore this next line
	  		//var current = el.item.index;

	  		//if you disable loop you have to comment this block
	  		var count = el.item.count-1;
	  		var current = Math.round(el.item.index - (el.item.count/2) - .5);

	  		if(current < 0) {
	  		  	current = count;
	  		}
	  		if(current > count) {
	  		  	current = 0;
	  		}

	  		//end block

	  		sync2
	  			.find(".owl-item")
	  			.removeClass("current")
	  			.eq(current)
	  			.addClass("current");
	  		var onscreen = sync2.find('.owl-item.active').length - 1;
	  		var start = sync2.find('.owl-item.active').first().index();
	  		var end = sync2.find('.owl-item.active').last().index();

	  		if (current > end) {
	  		  sync2.data('owl.carousel').to(current, 100, true);
	  		}
	  		if (current < start) {
	  		  	sync2.data('owl.carousel').to(current - onscreen, 100, true);
	  		}
	  	}

	  	function syncPosition2(el) {
	  		if(syncedSecondary) {
	  		  	var number = el.item.index;
	  		  	sync1.data('owl.carousel').to(number, 100, true);
	  		}
	  	}

	  	sync2.on("click", ".owl-item", function(e){
	  		e.preventDefault();
	  		var number = $(this).index();
	  		sync1.data('owl.carousel').to(number, 300, true);
	  	});
	});

	// Star Rating
	$(document).ready(function(){

		var i;

	  	/* 1. Visualizing things on Hover - See next part for action on click */
	  	$('.cs_stars .cs_star').on('mouseover', function(){
	  	  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

	  	  // Now highlight all the stars that's not after the current hovered star
	  	  $(this).parent().children('.cs_star').each(function(e){
	  	    if (e < onStar) {
	  	      $(this).addClass('hover');
	  	    }
	  	    else {
	  	      $(this).removeClass('hover');
	  	    }
	  	  });

	  	}).on('mouseout', function(){
	  	  $(this).parent().children('.cs_star').each(function(e){
	  	    $(this).removeClass('hover');
	  	  });
	  	});


	  	/* 2. Action to perform on click */
	  	$('.cs_stars .cs_star').on('click', function(){
	  	  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	  	  var stars = $(this).parent().children('.cs_star');

	  	  for (i = 0; i < stars.length; i++) {
	  	    $(stars[i]).removeClass('selected');
	  	  }

	  	  for (i = 0; i < onStar; i++) {
	  	    $(stars[i]).addClass('selected');
	  	  }

	  	  var ratingValue = parseInt($(this).closest('.cs_star_select').find('span.selected').last().data('value'), 10);

	  	  // Put on text field
	  	  $(this).closest('.cs_star_select').find('input.csfield_input').val(ratingValue);


	  	});


	});



})(jQuery)


/* WC Zoom Extend */
jQuery( function( $ ) {

	// wc_single_product_params is required to continue.
	if ( typeof wc_single_product_params === 'undefined' ) {


		//var wc_single_product_params = {"i18n_required_rating_text":"Please select a rating","review_rating_required":"yes","flexslider":{"rtl":false,"animation":"slide","smoothHeight":true,"directionNav":false,"controlNav":"thumbnails","slideshow":false,"animationSpeed":500,"animationLoop":false,"allowOneSlide":false},"zoom_enabled":"1","zoom_options":[],"photoswipe_enabled":"1","photoswipe_options":{"shareEl":false,"closeOnScroll":false,"history":false,"hideAnimationDuration":0,"showAnimationDuration":0},"flexslider_enabled":""};
	}

	/**
	 * Init zoom.
	 */
	function initZoomForTarget( zoomTarget  ) {

		zoomEnabled = true;

		// But only zoom if the img is larger than its container.
		if ( zoomEnabled ) {
			var zoom_options = $.extend( {
				touch: false,
				callback: function(){
					$(this).on('click',function(){
						$('.woocommerce-product-gallery__trigger').trigger('click');

						$(this).css('opacity',"0");
					});
				},
			}, wc_single_product_params.zoom_options );

			if ( 'ontouchstart' in document.documentElement ) {
				zoom_options.on = 'click';
			}

			zoomTarget.trigger( 'zoom.destroy' );
			zoomTarget.zoom( zoom_options );

			setTimeout( function() {
				if ( zoomTarget.find(':hover').length ) {
					zoomTarget.trigger( 'mouseover' );
				}
			}, 100 );
		}
	};

	// Init zoom images for gallery
	$('.woocommerce-product-gallery__image').each(function(i,el){
		initZoomForTarget( $(this) );
	});


} );
